#!/bin/bash
rm -Rf ./assets && \
go get github.com/qor/bindatafs && \
bindatafs --exit-after-compile=false assets && \
go run main.go compile && \
go build -tags 'bindatafs' -ldflags="-s -w" main.go && \
NODE_ENV=production yarn build && \
rsync --progress ./main shakal@shirykalov.ru:/home/shakal/app && \
rsync -a --progress ./public/ shakal@shirykalov.ru:/home/shakal/public/ && \
rm public/webpack/* && \
ssh shakal@shirykalov.ru "systemctl --user restart shakal"
