package kv

import (
	"github.com/dgraph-io/badger/v3"
)

type Engine struct {
}

func (e *Engine) Get(key []byte) (ret []byte, err error) {
	var item *badger.Item
	DB.View(func(txn *badger.Txn) (er error) {
		item, err = txn.Get(key)
		er = err
		return
	})
	if err != nil {
		return
	}
	item.Value(func(val []byte) error {
		ret = append([]byte{}, val...)

		return nil
	})
	return
}

func (e *Engine) Set(key []byte, body []byte) error {
	return DB.Update(func(txn *badger.Txn) error {
		return txn.Set(key, body)
	})
}

func (e *Engine) Delete(key []byte) error {
	return DB.Update(func(txn *badger.Txn) error {
		return txn.Delete(key)
	})
}
