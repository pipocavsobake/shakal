package kv

import (
	"io/ioutil"
	"os"

	"github.com/dgraph-io/badger/v3"
	yaml "gopkg.in/yaml.v2"
)

var DB *badger.DB
var Config Database
var Configs map[string]Database

func Init(filename string) {
	LongInit(filename, false)
}

func LongInit(filename string, managed bool) {
	Resume()
	if filename == "" {
		filename = "./kv/database.yml"
	}
	data, err := ioutil.ReadFile(filename)
	pErr(err)
	err = yaml.Unmarshal([]byte(data), &Configs)
	pErr(err)
	env := os.Getenv("RAILS_ENV")
	if env == "" {
		env = "development"
	}
	Config = Configs[env]
	opts := badger.DefaultOptions("")
	opts.Dir = *Config.Dir
	opts.ValueDir = *Config.ValueDir
	if managed {
		DB, err = badger.OpenManaged(opts)
	} else {
		DB, err = badger.Open(opts)
	}
	pErr(err)
}

func pErr(err error) {
	if err != nil {
		panic(err)
	}
}

func Resume() {
	if DB != nil {
		DB.Close()
	}
}
