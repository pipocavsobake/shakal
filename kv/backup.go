package kv

import "os"

func Backup(filename string) (err error) {
	Init("")
	w, e := os.Create(filename)
	if e != nil {
		return e
	}
	_, err = DB.Backup(w, 0)
	defer Resume()
	defer w.Close()
	return
}

func Restore(filename string) error {
	Init("")
	r, e := os.Open(filename)
	if e != nil {
		return e
	}
	defer Resume()
	defer r.Close()
	return DB.Load(r, 100)
}

func Drop() error {
	LongInit("", true)
	defer Resume()
	return DB.DropAll()
}
