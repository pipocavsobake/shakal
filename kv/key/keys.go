package key

import "fmt"

func For(model string, id int64) []byte {
	return []byte(fmt.Sprintf("%s:->%v", model, id))
}
