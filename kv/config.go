package kv

type Database struct {
	Dir      *string `yaml:"dir"`
	ValueDir *string `yaml:"value_dir"`
}
