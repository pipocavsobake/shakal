package cli

import (
	"log"
	"os"

	"github.com/Songmu/prompter"
	"github.com/urfave/cli"

	"gitlab.com/pipocavsobake/shakal/assets"
	"gitlab.com/pipocavsobake/shakal/kv"
)

var App *cli.App
var Serve bool
var Console bool
var Port int
var WebpackProd bool

const DEFAULT_PORT = 4834

func init() {
	App = cli.NewApp()
	App.EnableBashCompletion = true
	App.Name = "shakal"
	App.Usage = "shakal game"
	App.Version = "0.0.1"

	cli.VersionFlag = cli.BoolFlag{
		Name:  "version, v",
		Usage: "print only the version",
	}

	App.Before = func(c *cli.Context) error {
		Serve = false
		return nil
	}

	App.Flags = []cli.Flag{
		cli.BoolFlag{
			Name:        "webpack_prod",
			Usage:       "Webpack production mode",
			Destination: &WebpackProd,
		},
	}

	App.Commands = []cli.Command{
		{
			Name:    "serve",
			Aliases: []string{"server", "s", "listen", "start"},
			Usage:   "start http server",
			Flags: []cli.Flag{
				cli.IntFlag{
					Name:        "port",
					Usage:       "dev server port",
					EnvVar:      "PORT",
					Value:       DEFAULT_PORT,
					Destination: &Port,
				},
			},
			Action: func(c *cli.Context) error {
				Serve = true
				return nil
			},
		},
		{
			Name:    "console",
			Aliases: []string{"c", "shell"},
			Usage:   "start interactive shell",
			Action: func(c *cli.Context) error {
				Console = true
				return nil
			},
		},

		{
			Name:    "backup",
			Usage:   "backup to file",
			Aliases: []string{"b"},
			Action: func(c *cli.Context) error {
				if c.NArg() == 0 {
					return cli.NewExitError("no filepath provided.", 3)
				}
				filename := c.Args().Get(0)
				log.Println("dump backup to file:", filename)
				return kv.Backup(filename)
			},
		},
		{
			Name:    "restore",
			Usage:   "restore from backup file",
			Aliases: []string{"r"},
			Action: func(c *cli.Context) error {
				if c.NArg() == 0 {
					return cli.NewExitError("no filepath provided.", 3)
				}
				filename := c.Args().Get(0)
				log.Println("load backup from from file:", filename)
				return kv.Restore(filename)
			},
		},
		{
			Name:  "drop",
			Usage: "drop database",
			Action: func(c *cli.Context) error {
				if !prompter.YesNo("Do you really want to drop your database?", false) {
					return nil
				}
				log.Println("start dropping")
				return kv.Drop()
			},
		},
		{
			Name:  "compile",
			Usage: "Compile AssetFS",
			Action: func(c *cli.Context) error {
				//authconfig.Setup()
				assets.AssetFS.Compile()
				return nil
			},
		},
	}

	App.Action = func(c *cli.Context) error {
		Serve = true
		return nil
	}

}
func Run() {
	if err := App.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}
