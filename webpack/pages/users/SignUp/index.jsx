import React, { Component } from "react";
import PropTypes from "prop-types";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Input from "@mui/material/Input";
import InputLabel from "@mui/material/InputLabel";
import LockIcon from "@mui/icons-material/LockOutlined";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import { withStyles } from "@mui/styles";
import axios from "axios";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(488)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing(2)} ${theme.spacing(3)} ${theme
      .spacing(3)}px`
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
  submit: {
    marginTop: theme.spacing(3)
  }
});

class SignIn extends Component {
  state = {
    submitting: false,
    form: {
      username: "",
      password: "",
      confirm_password: ""
    }
  };
  submit = event => {
    event.preventDefault();
    this.setState({ submitting: true });
    axios
      .post("/auth/register", this.state.form)
      .then(resp => {
        console.log(resp);
      })
      .catch(err => {
        console.log(err.response);
        if (err.response.status === 307) {
          this.props.login();
          return;
        }
        alert(JSON.stringify(err.response.data));
      })
      .then(() => {
        this.setState({ submitting: false });
      });
  };

  checkRedirect() {
    if (this.props.currentUser) {
      this.props.history.push("/games");
    }
  }

  componentDidMount() {
    this.checkRedirect();
  }
  componentDidUpdate() {
    this.checkRedirect();
  }

  render() {
    const { classes } = this.props;
    const { username, password, confirm_password } = this.state.form;

    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Регистрирующая форма
          </Typography>
          <form
            className={classes.form}
            action="/auth/register"
            method="POST"
            onSubmit={this.submit}
          >
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="username">Игровое имя</InputLabel>
              <Input
                id="username"
                name="username"
                autoComplete="username"
                value={username}
                onChange={e =>
                  this.setState({
                    form: { ...this.state.form, username: e.target.value }
                  })
                }
                autoFocus
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Пароль</InputLabel>
              <Input
                name="password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={password}
                onChange={e =>
                  this.setState({
                    form: { ...this.state.form, password: e.target.value }
                  })
                }
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="confirm_password">И опять пароль</InputLabel>
              <Input
                name="cofirm_password"
                type="password"
                id="cofirm_password"
                autoComplete="current-password"
                value={confirm_password}
                onChange={e =>
                  this.setState({
                    form: {
                      ...this.state.form,
                      confirm_password: e.target.value
                    }
                  })
                }
              />
            </FormControl>
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={this.state.submitting}
            >
              Жми
            </Button>
          </form>
        </Paper>
      </main>
    );
  }
}
SignIn.propTypes = {
  classes: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser
  };
}

function mapDispatchToProps(dispatch) {
  return {
    login: () => dispatch({ type: "SIGN_IN" }),
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(
  withRouter(withStyles(styles)(SignIn))
);
