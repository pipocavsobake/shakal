import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import Select from "react-select";
import { withStyles } from "@mui/styles";
import Typography from "@mui/material/Typography";
import NoSsr from "@mui/material/NoSsr";
import TextField from "@mui/material/TextField";
import Paper from "@mui/material/Paper";
import Chip from "@mui/material/Chip";
import MenuItem from "@mui/material/MenuItem";
import CancelIcon from "@mui/icons-material/Cancel";
import axios from "axios";
import Autocomplete from '@mui/material/Autocomplete';
import CircularProgress from '@mui/material/CircularProgress';

const suggestions = [];

const styles = theme => ({
  root: {
    flexGrow: 1
  },
  input: {
    display: "flex",
    padding: 0
  },
  chip: {
    margin: `${theme.spacing.unit / 2}px ${theme.spacing.unit / 4}px`
  },
  chipFocused: {
    backgroundColor: theme.palette.type === "light"
        ? theme.palette.grey[300]
        : theme.palette.grey[700],
  },
  noOptionsMessage: {
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`
  },
  placeholder: {
    position: "absolute",
    left: 2,
    fontSize: 16
  },
  paper: {
    position: "absolute",
    zIndex: 1,
    marginTop: theme.spacing.unit,
    left: 0,
    right: 0
  },
  divider: {
    height: theme.spacing.unit * 2
  }
});

class IntegrationReactSelect extends React.Component {
  state = {
    single: null,
    multi: null,
    options: []
  };

  handleChange = name => value => {
    this.props.onChange(value);
    if (value.length > 2) {
      this.setState({ options: [] });
    }
  };
  onInputChange = value => {
    if (this.props.values && this.props.values.length > 2) {
      this.setState({
        options: []
      });
      return;
    }
    axios
      .get(`/users/search?q=${value}`)
      .then(resp => {
        this.setState({
          options: resp.data.data.map(({ id, username }) => ({
            value: id,
            label: username
          }))
        });
      })
      .catch(err => {
        console.error(err);
      });
  };

  loadOptions = (value, callback) => {
    axios
      .get(`/users/search?q=${value}`)
      .then(data => {
        callback(data.data);
      })
      .catch(err => {
        console.error(err);
      });
  };

  render() {
    const { classes, theme } = this.props;

    const selectStyles = {
      input: base => ({
        ...base,
        color: theme.palette.text.primary,
        "& input": {
          font: "inherit"
        }
      })
    };

    let options = this.state.options;
    let noOptions = "Нет таких";
    if (this.props.values && this.props.values.length > 2) {
      options = [];
      noOptions = "выбрано максимальное количество игроков";
    }
    const {loading} = this.state;

    return (
      <div className={classes.root}>
        <NoSsr>
          <div className={classes.divider} />
            <Autocomplete
              options={options}
              filterOptions={x => x}
              getOptionLabel={({label}) => label}
              multiple
              onInputChange={(event, newValue) => this.onInputChange(newValue)}
              value={this.props.values || []}
              onChange={(event, newValue) => this.handleChange("multi")(newValue) }
              noOptionsText={noOptions}
              renderInput={(params) => (
                <TextField
                  {...params}
                  label="Пиши, выбирай имена"
                  InputProps={{
                    ...params.InputProps,
                    endAdornment: (
                      <React.Fragment>
                        {loading ? <CircularProgress color="inherit" size={20} /> : null}
                        {params.InputProps.endAdornment}
                      </React.Fragment>
                    ),
                  }}
        />
      )}
            />
        </NoSsr>
      </div>
    );
  }
}

IntegrationReactSelect.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired
};

export default withStyles(styles, { withTheme: true })(IntegrationReactSelect);
