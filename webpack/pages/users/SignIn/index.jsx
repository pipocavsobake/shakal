import React, { Component } from "react";
import PropTypes from "prop-types";
import Avatar from "@mui/material/Avatar";
import Button from "@mui/material/Button";
import CssBaseline from "@mui/material/CssBaseline";
import FormControl from "@mui/material/FormControl";
import FormControlLabel from "@mui/material/FormControlLabel";
import Checkbox from "@mui/material/Checkbox";
import Input from "@mui/material/Input";
import InputLabel from "@mui/material/InputLabel";
import LockIcon from "@mui/icons-material/LockOutlined";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import { withStyles } from "@mui/styles";
import axios from "axios";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import Divider from '@mui/material/Divider';
const styles = theme => ({
  main: {
    width: "auto",
    display: "block", // Fix IE 11 issue.
    marginLeft: theme.spacing(3),
    marginRight: theme.spacing(3),
    [theme.breakpoints.up(448)]: {
      width: 400,
      marginLeft: "auto",
      marginRight: "auto"
    }
  },
  paper: {
    marginTop: theme.spacing(8),
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    padding: `${theme.spacing(2)} ${theme.spacing(3)} ${theme
      .spacing(3)}`
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main
  },
  form: {
    width: "100%", // Fix IE 11 issue.
    marginTop: theme.spacing(1)
  },
});

class SignIn extends Component {
  state = {
    submitting: false,
    form: {
      username: "",
      password: ""
    }
  };
  submit = event => {
    event.preventDefault();
    this.setState({ submitting: true });
    axios
      .post("/auth/login", this.state.form)
      .then(resp => {
        console.log(resp);
      })
      .catch(err => {
        console.log(err.response);
        if (err.response.status === 307) {
          this.props.login();
          return;
        }
        alert(JSON.stringify(err.response.data));
      })
      .then(() => {
        this.setState({ submitting: false });
      });
  };

  checkRedirect() {
    if (this.props.currentUser) {
      this.props.history.push("/games");
    }
  }

  componentDidMount() {
    this.checkRedirect();
  }
  componentDidUpdate() {
    this.checkRedirect();
  }

  render() {
    const { classes } = this.props;
    const { username, password } = this.state.form;

    return (
      <main className={classes.main}>
        <CssBaseline />
        <Paper className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form
            className={classes.form}
            action="/auth/login"
            method="POST"
            onSubmit={this.submit}
          >
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="username">Игровое имя</InputLabel>
              <Input
                id="username"
                name="username"
                autoComplete="username"
                value={username}
                onChange={e =>
                  this.setState({
                    form: { ...this.state.form, username: e.target.value }
                  })
                }
                autoFocus
              />
            </FormControl>
            <FormControl margin="normal" required fullWidth>
              <InputLabel htmlFor="password">Пароль</InputLabel>
              <Input
                name="password"
                type="password"
                id="password"
                autoComplete="current-password"
                value={password}
                onChange={e =>
                  this.setState({
                    form: { ...this.state.form, password: e.target.value }
                  })
                }
              />
            </FormControl>
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
              disabled={this.state.submitting}
            >
              Войти
            </Button>
            <Button
              sx={{mt: 1}}
              fullWidth
              variant="contained"
              onClick={() => {
                this.props.history.push("/users/sign_up");
              }}
            >
              Я тут впервые
            </Button>
          </form>
        </Paper>
      </main>
    );
  }
}
SignIn.propTypes = {
  classes: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    currentUser: state.currentUser
  };
}

function mapDispatchToProps(dispatch) {
  return {
    login: () => dispatch({ type: "SIGN_IN" }),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(
  withRouter(withStyles(styles)(SignIn))
);
