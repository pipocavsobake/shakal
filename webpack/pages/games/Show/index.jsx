import React, { Component } from "react";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import axios from "axios";
import Game from "~/containers/Game";
import Dialog from "~/components/Dialog";
class Show extends Component {
  state = {
    game: null,
    data: null,
    dialog: false,
    wsclosed: true
  };

  loadGame = () => {
    axios
      .get(`/api/games/${this.props.match.params.id}`)
      .then(resp => {
        this.setState({ game: resp.data.game, data: resp.data.data });
      })
      .catch(err => {
        alert(err.response.error);
      });
  };

  connectToSocket = () => {
    if (!this.state.wsclosed) return;
    const host = window.location.host;
    const prot = host.match(/^localhost/) ? "ws" : "wss";
    this.ws = new WebSocket(
      `${prot}://${host}/ws/${this.props.match.params.id}`
    );
    this.ws.onopen = () => {
      this.setState({ wsclosed: false });
    };
    this.ws.onmessage = event => {
      const data = JSON.parse(event.data);
      data.my_player = this.state.data.my_player;
      this.setState({ data });
    };
    this.ws.onclose = () => {
      this.setState({ wsclosed: true });
    };
  };

  componentDidMount() {
    this.loadGame();
    this.wsInterval = setInterval(() => {
      if (this.state.wsclosed) this.connectToSocket();
    }, 1000);
  }

  componentWillUmount() {
    this.ws.onclose = () => {};
    this.ws.close();
    if (this.wsInterval) clearInterval(this.wsInterval);
  }

  cellClick = (cell, event) => {
    const { data } = this.state;
    if (
      this.state.pending ||
      data.my_player !== data.current_player ||
      !cell.achievable ||
      data.selected.length === 0
    ) {
      return;
    }
    const action = {
      type: "move",
      chip_ids: data.selected,
      to: cell.id
    };
    const level = data.desk[data.current_cell].chips.find(
      chip => chip.id === data.selected[0]
    ).level;
    if (
      cell.type === "marsh" &&
      cell.chips.filter(chip => chip.level === level).length > 1
    ) {
      this.setState({ action, dialog: true });
      return;
    }

    this.sendAction(action);
  };

  chipClick = (chip, event) => {
    if (
      this.state.pending ||
      (chip.type !== "coin" &&
        (chip.owner !== this.state.data.my_player ||
          chip.owner !== this.state.data.current_player))
    ) {
      return;
    }
    if (!chip.selectable) return;
    event.stopPropagation();
    this.setState({ pending: true });
    this.sendAction({ type: "select", id: chip.id });
    //console.log("chipClick", chip, event);
  };

  sendAction = action => {
    axios
      .put(`/api/games/${this.props.match.params.id}`, action)
      .then(resp => {
        this.setState({ data: resp.data.data });
      })
      .catch(err => {
        console.error(err);
      })
      .then(() => {
        this.setState({ pending: false, action: null, dialog: false });
      });
  };

  confirmAction = () => {
    this.sendAction(this.state.action);
  };

  dropAction = () => {
    this.setState({ action: null, dialog: false });
  };

  render() {
    const game = this.state.game;
    if (!game) return <div />;
    const label = `${game.users.map(u => u.username).join(", ")} — ${game.id}`;
    return (
      <div>
        <Paper>
          <Typography>{label}</Typography>
          {this.state.wsclosed ? (
            <Typography>Соединение с сервером прервано</Typography>
          ) : null}
          <Dialog
            open={this.state.dialog}
            onYes={this.confirmAction}
            onNo={this.dropAction}
            onClose={this.dropAction}
          />
          <Game
            data={this.state.data}
            onCellClick={this.cellClick}
            onChipClick={this.chipClick}
          />
        </Paper>
      </div>
    );
  }
}

export default Show;
