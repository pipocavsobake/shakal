import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import Button from "@mui/material/Button";
import axios from "axios";
import { push } from "connected-react-router";
import { connect } from "react-redux";

import UserSearch from "~/pages/users/Search";

const styles = theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(2),
    [theme.breakpoints.up('sm')]: {
      paddingLeft: theme.spacing(3),
      paddingRight: theme.spacing(3),
    },
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2
  },
  button: {
    margin: theme.spacing.unit
  }
});

class New extends Component {
  state = {
    form: { users: null },
    submitting: false
  };
  submit = () => {
    this.setState({ submitting: true });
    axios
      .post("/api/games", {
        ...this.state.form,
        users: this.state.form.users.map(v => v.value)
      })
      .then(resp => {
        this.props.select(`/games/${resp.data.id}`);
      })
      .catch(err => {
        alert(err.response.data);
      })
      .then(() => {
        this.setState({ submitting: false });
      });
  };
  render() {
    const { classes } = this.props;
    return (
      <div>
        <Paper className={classes.Root} elevation={1}>
          <Typography variant="h5" component="h3">
            Тут надо кого-то пригласить
          </Typography>
          <UserSearch
            values={this.state.form.users}
            onChange={values =>
              this.setState({ form: { ...this.state.form, users: values } })
            }
          />
        </Paper>
        {this.state.form.users && this.state.form.users.length > 0 ? (
          <Button
            variant="contained"
            disabled={this.state.submitting}
            color="primary"
            className={classes.button}
            onClick={this.submit}
          >
            Отправить приглашения
          </Button>
        ) : null}
      </div>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    select: path => dispatch(push(path))
  };
}

export default connect(null, mapDispatchToProps)(withStyles(styles)(New));
