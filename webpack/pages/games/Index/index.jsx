import React, { Component } from "react";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Typography from "@mui/material/Typography";
import axios from "axios";
import Button from "@mui/material/Button";
import { Link } from "react-router-dom";
class Index extends Component {
  state = {
    games: [],
    total: 0
  };
  loadGames = (page = 1) => {
    axios
      .get(`/api/games?page=${page}`)
      .then(resp => {
        this.setState({ total: resp.data.total, games: resp.data.data });
      })
      .catch(err => {
        console.error(err);
      });
  };

  componentDidMount() {
    this.loadGames();
  }

  label(game) {
    return `${game.users.map(u => u.username).join(", ")} — ${game.id}`;
  }
  render() {
    return (
      <Paper>
        <Typography>{this.state.total}</Typography>
        <Grid container spacing={16}>
          {this.state.games.map(game => (
            <Grid item key={game.id} xs={12} sm={6} md={4} lg={3} xl={2}>
              <Link to={`/games/${game.id}`}>
                <Button variant="contained">
                  <Typography>{this.label(game)}</Typography>
                </Button>
              </Link>
            </Grid>
          ))}
        </Grid>
      </Paper>
    );
  }
}

export default Index;
