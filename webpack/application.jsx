import React from "react";
import { render } from "react-dom";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import { Provider } from "react-redux";
import App from "./containers/App";
import reducer from "./reducer";
import { AppContainer } from "react-hot-loader";
import websocket from "@giantmachines/redux-websocket";
import queryString from "query-string";
import { createBrowserHistory } from "history";
import { routerMiddleware } from "connected-react-router";
import { ConnectedRouter } from "connected-react-router";
import axiosMiddleware from "redux-axios-middleware";
import createSagaMiddleware from "redux-saga";
import SignIn, { StartSignIn } from "./sagas/signIn";
import axios from "axios";
const client = axios.create({
  responseType: "json"
});

const init = () => {
  const history = createBrowserHistory();
  const sagaMiddleware = createSagaMiddleware();
  const store = createStore(
    reducer(history),
    composeWithDevTools(
      applyMiddleware(websocket()),
      applyMiddleware(routerMiddleware(history)),
      applyMiddleware(axiosMiddleware(client)),
      applyMiddleware(sagaMiddleware)
    )
  );
  window.store = store;
  sagaMiddleware.run(SignIn);
  sagaMiddleware.run(StartSignIn);

  const r = () => {
    render(
      <AppContainer>
        <Provider store={store}>
          <App />
        </Provider>
      </AppContainer>,
      document.getElementById("root")
    );
  };

  r();

  if (module.hot) module.hot.accept("./containers/App", r);
};

document.addEventListener("DOMContentLoaded", init);
