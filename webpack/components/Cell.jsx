import React from "react";
import "./cell.sass";
import _ from "lodash";
import PropTypes from "prop-types";

const klass = props => {
  if (props.type === 0) {
    return "cell closed" + (props.achievable ? " achievable" : "");
  }
  return (
    _.filter(["level", "direction", "owner"], k => props[k] !== undefined)
      .map(k => `${k}-${props[k]}`)
      .join(" ") +
    ` cell ${props.type}` +
    `${props.achievable ? " achievable" : ""}`
  );
};

const Cell = props => {
  return (
    <div
      className={klass(props)}
      onClick={props.onClick}
      data-level={props.level}
    >
      {props.children}
    </div>
  );
};

Cell.propTypes = {
  type: PropTypes.string.isRequired,
  achievable: PropTypes.bool.isRequired,
  level: PropTypes.number,
  owner: PropTypes.number,
  direction: PropTypes.number
};

export default Cell;
