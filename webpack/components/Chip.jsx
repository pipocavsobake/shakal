import React from "react";
import PropTypes from "prop-types";
import "./chip.sass";

const Chip = props => {
  const clsName = `chip type-${props.type} owner-${props.owner} ${
    props.selected ? "selected" : ""
  }`;
  return (
    <div className={clsName} onClick={props.onClick} data-level={props.level} />
  );
};

Chip.propTypes = {
  type: PropTypes.string.isRequired,
  onClick: PropTypes.func,
  selected: PropTypes.bool.isRequired,
  owner: PropTypes.number,
  level: PropTypes.number
};

export default Chip;
