import { stopSubmit, SubmissionError } from "redux-form";
import { put, call, takeLatest } from "redux-saga/effects";
import axios from "axios";

//function* login(action) {
//yield put({type: 'SIGN_IN',
//}
function* checkLogin() {
  try {
    const resp = yield axios.get("/api/users/me");
    yield put({ type: "LOGIN", user: resp.data });
  } catch (err) {
    console.error(err);
  }
}

function* login(action) {
  yield call(checkLogin);
}

function* SignIn() {
  yield takeLatest("SIGN_IN", login);
}

export function* StartSignIn() {
  yield takeLatest("LOADED", checkLogin);
}

export default SignIn;
