import React, { Component } from "react";
import { Route, Switch, BrowserRouter } from "react-router-dom";
import Header from "~/containers/Header";

import HomeIndexPage from "~/pages/home/Index";
import GamesIndexPage from "~/pages/games/Index";
import GamesNewPage from "~/pages/games/New";
import GamesShowPage from "~/pages/games/Show";
import UsersSignInPage from "~/pages/users/SignIn";
import UsersSignUpPage from "~/pages/users/SignUp";
import { connect } from "react-redux";
import { createTheme, ThemeProvider } from '@mui/material/styles';
const defaultTheme = createTheme();

class App extends Component {
  componentDidMount() {
    this.props.mount();
  }
  render() {
    return (
      <ThemeProvider theme={defaultTheme}>
        <BrowserRouter>
        <Header />
          <div className="container">
            <Switch>
              <Route exact path="/" component={HomeIndexPage} />
              <Route exact path="/games" exact component={GamesIndexPage} />
              <Route exact path="/games/new" exact component={GamesNewPage} />
              <Route exact path="/games/:id" component={GamesShowPage} />
              <Route exact path="/users/sign_in" component={UsersSignInPage} />
              <Route exact path="/users/sign_up" component={UsersSignUpPage} />
              <Route render={() => <div>Miss</div>} />
            </Switch>
          </div>
        </BrowserRouter>
      </ThemeProvider>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return { mount: () => dispatch({ type: "LOADED" }) };
}

function mapStateToProps(state) {
  return { switchKey: state.router.location.key };
}

export default connect(mapStateToProps, mapDispatchToProps)(App);
