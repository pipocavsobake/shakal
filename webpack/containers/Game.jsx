import React from "react";
import Cell from "../components/Cell";
import Chip from "../components/Chip";
import _ from "lodash";
import "./game.sass";

const GameComponent = props => {
  if (!props.data) return <div />;
  const player = props.data.current_player;
  const myPlayer = props.data.my_player;
  const extraChipProps = chip => {
    if (chip.selectable)
      return {
        onClick: e => {
          e.stopPropagation();
          return selector(chip.id);
        }
      };
    return {};
  };
  const extraCellProps = cell => {
    if (!cell.achievable || selected.length === 0 || player !== myPlayer)
      return {};
    const lvl = selected[0].level;
    const extra =
      typeof lvl === "number" && cell.level ? { level: lvl + 1 } : {};
    return {
      onClick: () =>
        mover({ to: cell.id, ...extra, chipIds: _.map(selected, "id") })
    };
  };
  const plcls = pl => `result player-${pl}`;
  const pcls = `bar-player player-${player}`;
  const youcls = `bar-player my-player player-${myPlayer}`;
  return (
    <div className="shakal">
      <div className="shakal-bar">
        <div className={pcls}>Текущий</div>
        <div className={youcls}>Вы</div>
        <div className="results">
          {_.map(props.data.results, (result, pl) => {
            return (
              <div key={pl} className={plcls(pl)}>
                {result}
              </div>
            );
          })}
        </div>
      </div>
      <div className="desk">
        {props.data.desk.map(cell => {
          return (
            <Cell
              key={cell.id}
              {...cell}
              onClick={e => props.onCellClick(cell, e)}
            >
              {_.map(cell.chips, chip => {
                return (
                  <Chip
                    key={chip.id}
                    {...chip}
                    onClick={e => props.onChipClick(chip, e)}
                  />
                );
              })}
            </Cell>
          );
        })}
      </div>
    </div>
  );
};

export default GameComponent;
