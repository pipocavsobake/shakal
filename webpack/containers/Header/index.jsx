import React, { Component } from "react";
import { withStyles } from "@mui/styles";
import AppBar from "@mui/material/AppBar";
import Button from "@mui/material/Button";
import IconButton from "@mui/material/IconButton";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import MenuIcon from "@mui/icons-material/Menu";
import PropTypes from "prop-types";

import Menu from "@mui/material/Menu";
import MenuItem from "@mui/material/MenuItem";

import * as actions from "~/actions";
import { connect } from "react-redux";

import { Link, withRouter } from 'react-router-dom';

import axios from "axios";

const styles = {
  root: {
    flexGrow: 1
  },
  grow: {
    flexGrow: 1,
    cursor: "pointer"
  },
  menuButton: {
    marginLeft: -12,
    marginRight: 20
  }
};

class Header extends Component {
  state = {
    pending: false,
    anchorEl: null,
  };
  logout() {
    if (this.state.pending) return;
    this.setState({ pending: true });
    axios
      .delete("/auth/logout")
      .then(resp => {
        this.props.logout();
        this.props.history.push("/users/sign_in");
      })
      .catch(err => {
        console.error(err);
        alert("Ты отсюда не уйдёшь");
      })
      .then(() => this.setState({ pending: false }));
  }
  closeMenu = () => {
    this.setState({anchorEl: null})
  }

  selectMenuItem(path, needUser) {
    const {history, currentUser} = this.props;
    if (currentUser || !needUser) {
      history.push(path);
    } else {
      history.push("/users/sign_in");
    }
    this.closeMenu();
  }

  renderMenu() {
    const { anchorEl } = this.state;
    return (
      <Menu
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={this.closeMenu}
      >
        <MenuItem onClick={() => this.selectMenuItem("/games/new", true)} >
          Новая игра
        </MenuItem>
        <MenuItem onClick={() => this.selectMenuItem("/games", false)}>
          Мои игры
        </MenuItem>
      </Menu>
    );
  }
  render() {
    const { classes } = this.props;
    const logoutText = `Меня зовут ${this.props.currentUser &&
      this.props.currentUser.username} и я хочу выйти`;
    return (
      <header className={classes.root}>
        <AppBar position="static">
          <Toolbar>
            <IconButton
              className={classes.menuButton}
              color="inherit"
              aria-label="Menu"
              onClick={e => this.setState({anchorEl: e.target})}
            >
              <MenuIcon />
            </IconButton>
            <Typography
              variant="h6"
              color="inherit"
              className={classes.grow}
              onClick={() => this.props.history.push("/")}
            >
              Шакал
            </Typography>
            {!this.props.currentUser ? (
              <Button
                onClick={() => this.props.history.push("/users/sign_in")}
                color="inherit"
              >
                Войти
              </Button>
            ) : (
              <Button onClick={() => this.logout()} color="inherit">
                {logoutText}
              </Button>
            )}
            {this.renderMenu()}
          </Toolbar>
        </AppBar>
      </header>
    );
  }
}

Header.propTypes = {
  classes: PropTypes.object.isRequired
};

const mapStateToProps = ({ currentUser }) => {
  return {
    currentUser
  };
};

const mapDispatchToProps = dispatch => {
  return {
    logout: () => dispatch({ type: "LOGOUT" })
  };
};

export default withRouter(
  connect(mapStateToProps, mapDispatchToProps)(
    withStyles(styles)(Header)
  )
);
