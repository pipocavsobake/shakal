import { initGame } from "../core";
import * as actionTypes from "./types";

export const select = (field_id, object_id) => {
  return {
    type: "SELECT",
    field_id,
    object_id
  };
};

// from, to, object_id, with_monets
export const move = args => {
  return {
    type: "MOVE",
    ...args
  };
};

export const initGAME = game => {
  return { type: "server/initGame", game };
};

export const mainMenuToggle = el => {
  return { type: actionTypes.MAIN_MENU_TOGGLE, data: el };
};

export const sign_in = data => {
  return {
    type: actionTypes.SIGN_IN,
    payload: {
      request: {
        url: "/auth/login",
        method: "POST",
        data: data
      }
    }
  };
};
