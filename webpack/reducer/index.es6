import { initGame } from "../core";
import _ from "lodash";
import Game from "../core/Game";
import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";
import * as actionTypes from "~/actions/types";
import { LOCATION_CHANGE } from "connected-react-router";
import { reducer as formReducer } from "redux-form";

const defaultState = initGame();
const defaultServer = { myPlayer: null, ok: false };

export const game = (state = defaultState, action) => {
  if (action.type === "initGame") return action.game;
  if (action.type === "SELECT") {
    const target = state.chips[action.id];
    const isCoin = target.type === "coin";
    const isShip = target.type === "ship";
    return Object.assign({}, state, {
      chips: _.map(state.chips, (chip, id) => {
        if (isShip && chip.owner === state.player && chip.cell === target.cell)
          return Object.assign({}, chip, { selected: true });
        if (id === action.id)
          return Object.assign({}, chip, { selected: !chip.selected });
        if (!isCoin && chip.type === "coin" && chip.cell != target.cell)
          return Object.assign({}, chip, { selected: false });
        if (isCoin || chip.type === "coin") return chip;
        return Object.assign({}, chip, { selected: false });
      })
    });
  }
  if (action.type !== "MOVE") return state;
  return new Game(state, action).newState();
};

const myPlayer = (state = null, action) => {
  if (action.type === "JOIN" && action.you === "true") {
    if (action.watcher === "true") return "watcher";
    return parseInt(action.index);
  }
  if (action.type === "SET_PLAYER") return action.data;
  return state;
};

const connectedPlayers = (state = 0, action) => {
  if (action.type === "JOIN") return state++;
  if (action.type === "DISCONNECT") return state--;
  return state;
};

const server = (state = "new", action) => {
  if (action.type === "server/page") return "new";
  if (action.type.match(/^server/)) return "pending";
  if (action.type === "SET_PLAYER") return "noDesk";
  if (
    action.type === "initGame" ||
    action.type === "MOVE" ||
    action.type === "SELECT"
  )
    return "ok";
  return state;
};

const menu = (state = { anchorEl: null }, action) => {
  switch (action.type) {
    case actionTypes.MAIN_MENU_TOGGLE:
      return { ...state, anchorEl: action.data };
    case LOCATION_CHANGE:
      return { ...state, anchorEl: null };
    default:
      return state;
  }
};

const currentUser = (state = null, action) => {
  switch (action.type) {
    case "LOGIN":
      return action.user;
    case "LOGOUT":
      return null;
    default:
      return state;
  }
};

const data = (state = null, action) => {
  switch (action.type) {
    case "NEW_DATA":
      return action.data;
    default:
      return state;
  }
};

export default history =>
  combineReducers({
    currentUser,
    game,
    data,
    server,
    myPlayer,
    menu,
    connectedPlayers,
    router: connectRouter(history),
    form: formReducer
  });
