package main

import (
	"fmt"
	"log"

	"github.com/d4l3k/go-pry/pry"

	"gitlab.com/pipocavsobake/shakal/compressor"
	"gitlab.com/pipocavsobake/shakal/kv"

	"gitlab.com/pipocavsobake/shakal/cli"
	"gitlab.com/pipocavsobake/shakal/routes"

	"github.com/gin-contrib/static"
	webpack "github.com/go-webpack/webpack"
)

func main() {
	cli.Run()
	if cli.Console {
		pry.Pry()
	}
	if !cli.Serve {
		return
	}

	kv.Init("")
	defer kv.Resume()

	webpack.DevHost = "localhost:7310"
	webpack.Plugin = "manifest"
	webpack.Verbose = false
	log.Println("webpack prod mode:", cli.WebpackProd)
	webpack.Init(!cli.WebpackProd)

	engine := compressor.Compress(&kv.Engine{})
	r := routes.Router(&engine)

	r.Use(static.Serve("/webpack", static.LocalFile("/webpack", false)))

	port := cli.Port
	if port == 0 {
		port = cli.DEFAULT_PORT
	}
	log.Fatal(r.Run(fmt.Sprintf(":%v", port)))
}
