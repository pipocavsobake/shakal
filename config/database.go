package config

import (
	"io/ioutil"
	"log"
	"os"
	"strconv"
	"strings"

	yaml "gopkg.in/yaml.v2"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"

	"gitlab.com/pipocavsobake/shakal/lib/utils"
)

// Database config
type Database struct {
	Adapter  *string
	Host     *string
	Encoding *string
	Port     *int
	Pool     *int
	Username *string
	Password *string
	Db       *string `yaml:"database"`
}

// DB Gorm DB
var DB *gorm.DB

// Config database config data
var Config Database

// Configs possible DB configs
var Configs map[string]Database

func init() {
	var err error
	var databaseConfigPath string
	if databaseConfigPath = os.Getenv("DATABASE_YML"); databaseConfigPath == "" {
		databaseConfigPath = "./config/database.yml"
	}

	log.Println("try to open", databaseConfigPath)

	dat, err := ioutil.ReadFile(databaseConfigPath)
	utils.PErr(err)

	err = yaml.Unmarshal([]byte(dat), &Configs)
	utils.PErr(err)

	env := os.Getenv("GO_ENV")
	if env == "" {
		env = "development"
	}
	Config = Configs[env]

	if *Config.Adapter == "postgresql" {
		connstr := make([]string, 0)
		if Config.Host != nil {
			connstr = append(connstr, "host="+*Config.Host)
		}
		if Config.Port != nil {
			connstr = append(connstr, "port="+strconv.Itoa(*Config.Port))
		}
		if Config.Username != nil {
			connstr = append(connstr, "user="+*Config.Username)
		}
		if Config.Db != nil {
			connstr = append(connstr, "dbname="+*Config.Db)
		}
		if Config.Password != nil {
			connstr = append(connstr, "password="+*Config.Password)
		}
		connstr = append(connstr, "sslmode=disable")
		cs := strings.Join(connstr, " ")
		log.Printf("connecting to pg, conf: %v", cs)
		DB, err = gorm.Open(postgres.Open(cs), &gorm.Config{
			Logger: logger.Default.LogMode(logger.Info),
		})
		if err != nil {
			panic(err)
		}
	} else {
		panic("Unknown DB adapter: " + *Config.Adapter)
	}
	//log.Println("enable log mode")
	//DB.LogMode(true)
	db, err := DB.DB()
	if err != nil {
		panic(err)
	}
	db.SetMaxOpenConns(20)
}
