package config

import (
	"github.com/gorilla/sessions"

	"os"
)

var SessionStore *sessions.CookieStore

func init() {
	key := os.Getenv("SESSION_KEY")
	if key == "" {
		key = "don't use it in production"
	}
	SessionStore = sessions.NewCookieStore([]byte(key))
}
