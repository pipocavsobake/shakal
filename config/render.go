package config

import (
	"encoding/json"
	"html/template"

	//"github.com/davecgh/go-spew/spew"

	webpack "github.com/go-webpack/webpack"
	eztemplate "github.com/michelloworld/ez-gin-template"
)

var Render eztemplate.Render
var FuncMap template.FuncMap

func init() {
	Render = eztemplate.New()
	FuncMap = template.FuncMap{
		"asset": webpack.AssetHelper,
		//"csrf":  auth.CSRFHelper,
		"json": func(v interface{}) template.JS {
			a, _ := json.Marshal(v)
			return template.JS(a)
		},
	}
	Render.TemplateFuncMap = FuncMap
	Render = Render.Init()
}
