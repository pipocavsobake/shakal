package auth

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"

	"github.com/davecgh/go-spew/spew"
	"github.com/volatiletech/authboss/v3"
	"gorm.io/gorm"
)

var AuthFailError = errors.New("auth fail")

type ErrorHandler struct {
	LogWriter authboss.Logger
}

type SerializableError struct {
	Username *string `json:"username"`
	Password *string `json:"password"`
}

func (e ErrorHandler) Wrap(handler func(w http.ResponseWriter, r *http.Request) error) http.Handler {
	return errorHandler{
		Handler:   handler,
		LogWriter: e.LogWriter,
	}
}

type errorHandler struct {
	Handler   func(w http.ResponseWriter, r *http.Request) error
	LogWriter authboss.Logger
}

// ServeHTTP handles errors
func (e errorHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	err := e.Handler(w, r)
	if err == nil {
		return
	}

	if errors.Is(err, gorm.ErrRecordNotFound) {
		w.WriteHeader(404)
		msg := "user not found"
		se := SerializableError{Username: &msg}
		data, er := json.Marshal(se)
		if er != nil {
			panic(er)
		}
		w.Write(data)
	} else if err == AuthFailError {
		w.WriteHeader(422)
		w.Write([]byte("auth fail"))
	} else {
		spew.Dump(err)
		w.WriteHeader(400)
	}

	e.LogWriter.Error(fmt.Sprintf("request error from (%s) %s: %+v", r.RemoteAddr, r.URL.String(), err))
}
