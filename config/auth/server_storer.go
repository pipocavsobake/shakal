package auth

import (
	"context"

	"github.com/volatiletech/authboss/v3"
	"gitlab.com/pipocavsobake/shakal/app/models"
)

type ServerStorer struct{}

func (serverStorer *ServerStorer) Load(ctx context.Context, key string) (authboss.User, error) {
	var user models.User
	if err := models.DB.Where("username = ?", key).First(&user).Error; err != nil {
		return nil, err
	}
	return &user, nil
}

func (serverStorer *ServerStorer) Save(ctx context.Context, userInterface authboss.User) error {
	user := userInterface.(*models.User)
	if user.ID == 0 {
		return authboss.ErrUserNotFound
	}
	return models.DB.Save(user).Error
}

func (serverStorer *ServerStorer) New(ctx context.Context) authboss.User {
	return &models.User{}
}

func (serverStorer *ServerStorer) Create(ctx context.Context, user authboss.User) error {
	u := user.(*models.User)
	return models.DB.Save(u).Error
}
