package auth

import (
	"net/http"

	"github.com/gorilla/sessions"
	"github.com/volatiletech/authboss/v3"
	_ "github.com/volatiletech/authboss/v3/auth"
	"github.com/volatiletech/authboss/v3/defaults"
	_ "github.com/volatiletech/authboss/v3/logout"

	"gitlab.com/pipocavsobake/shakal/config/auth/abclientstate"
	_ "gitlab.com/pipocavsobake/shakal/config/auth/register"

	"os"
)

var AuthBoss *authboss.Authboss
var RootUrl string

func init() {
	if RootUrl == "" {
		RootUrl = "http://localhost:4834"
	}
	secret := os.Getenv("SECRET_KEY_BASE")
	if secret == "" {
		secret = "don't use me in production"
	}
	cookieStore := abclientstate.NewCookieStorer([]byte(secret), nil)
	cookieStore.HTTPOnly = false
	cookieStore.Secure = false

	sessionStore := abclientstate.NewSessionStorer("kaka", []byte(secret), nil)
	cstore := sessionStore.Store.(*sessions.CookieStore)
	cstore.Options.HttpOnly = false
	cstore.Options.Secure = false

	AuthBoss = authboss.New()
	AuthBoss.Config.Storage.Server = &ServerStorer{}
	AuthBoss.Config.Storage.SessionState = sessionStore
	AuthBoss.Config.Storage.CookieState = cookieStore
	AuthBoss.Config.Paths.RootURL = RootUrl
	AuthBoss.Config.Paths.Mount = "/auth"
	AuthBoss.Config.Modules.RegisterPreserveFields = []string{"usesrname"}

	AuthBoss.Config.Core.ViewRenderer = defaults.JSONRenderer{}
	defaults.SetCore(&AuthBoss.Config, true, true)
	AuthBoss.Config.Core.ErrorHandler = ErrorHandler{AuthBoss.Config.Core.Logger}
	AuthBoss.Events.After(authboss.EventAuthFail, authboss.EventHandler(func(w http.ResponseWriter, r *http.Request, handled bool) (bool, error) {
		return false, AuthFailError
	}))

	usernameRule := defaults.Rules{
		FieldName: "username", Required: true,
		MinLength: 4,
	}

	passwordRule := defaults.Rules{
		FieldName: "password", Required: true,
		MinLength: 4,
	}

	AuthBoss.Config.Core.BodyReader = defaults.HTTPBodyReader{
		ReadJSON:    true,
		UseUsername: true,
		Rulesets: map[string][]defaults.Rules{
			"register":    {usernameRule, passwordRule},
			"recover_end": {passwordRule},
		},
		Confirms: map[string][]string{
			"register":    {"password", authboss.ConfirmPrefix + "password"},
			"recover_end": {"password", authboss.ConfirmPrefix + "password"},
		},
		Whitelist: map[string][]string{
			"register": []string{"username", "password"},
		},
	}
	if err := AuthBoss.Init(); err != nil {
		panic(err)
	}
}
