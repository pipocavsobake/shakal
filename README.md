# Шакал
React, Redux, Webpack

# Запуск

```bash
yarn install
yarn start
```

# Собираем
```bash
NODE_ENV=production ./node_modules/.bin/webpack
```


# Go тесты
```
GO_ENV=test DATABASE_YML=$PWD/config/database.yml VIEW_PATH=$PWD/app/views go test ./... -v
```

## TODO
1. Пофиксить github.com/qor/render: добавить warning на каждый пропущенный error в методе
```go
func (tmpl *Template) Render(templateName string, obj interface{}, request *http.Request, writer http.ResponseWriter) (template.HTML, error)
```
Самый важный, который съел мне весь мозг тут (commit 63566e46f01b134ae9882a59a06518e82a903231, template.go:122:124)
```go
if err = t.Execute(&tpl, obj); err == nil {
  return template.HTML(tpl.String()), nil
}
```
2. Обновить github.com/go-webpack/webpack: загрузка manifest не использует параметр FsPath (commit 2f6187f39c5b58376a0ed8b7a5c205a81fe53f90, reader/manifest/manifest.go:14)
