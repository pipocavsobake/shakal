# docker build -t golang_with_node - < Dockerfile
FROM golang:1.17.3-buster

RUN curl -sS https://dl.yarnpkg.com/debian/pubkey.gpg | apt-key add -
RUN echo "deb https://dl.yarnpkg.com/debian/ stable main" | tee /etc/apt/sources.list.d/yarn.list
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash -
RUN apt-get update >/dev/null
RUN apt-get install -y yarn nodejs openssh-client rsync >/dev/null
