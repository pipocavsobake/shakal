package models

type UserGame struct {
	ID     int64  `gorm:"primary_key" json:"id"`
	UserID *int64 `gorm:"type:bigint REFERENCES users(id);not null;unique_index:idx_user_id_game_id" json:"user_id"`
	GameID *int64 `gorm:"type:bigint REFERENCES games(id);not null;unique_index:idx_user_id_game_id" json:"game_id"`
}
