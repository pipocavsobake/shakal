package models

import (
	"encoding/json"
	"strconv"
	"testing"

	"github.com/buger/jsonparser"

	"gitlab.com/pipocavsobake/shakal/lib/utils"
)

func createFakeGame() Game {
	return Game{
		ID: 1,
		Users: []*User{
			&User{Username: "1", ID: 1},
		},
	}
}

func TestGameJson(t *testing.T) {
	game := createFakeGame()
	realUser := game.Users[0]
	bs, err := json.Marshal(game)
	utils.PErr(err)
	foundKeys := make(map[string]bool)
	jsonparser.ObjectEach(bs, func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
		switch string(key) {
		case "id":
			if id, err := strconv.ParseInt(string(value), 10, 64); err != nil {
				utils.PErr(err)
			} else {
				if id != game.ID {
					t.Error("id != game.ID", id, game.ID)
				} else {
					foundKeys["id"] = true
				}
			}
		case "users":
			if dataType != jsonparser.Array {
				t.Error("users is not array", string(value))
			} else {
				jsonparser.ArrayEach(value, func(userValue []byte, userDataType jsonparser.ValueType, userOffset int, userErr error) {
					utils.PErr(userErr)
					user := User{}
					utils.PErr(json.Unmarshal(userValue, &user))
					if user.ID != realUser.ID {
						t.Error("user.ID != realUser.ID", user.ID, realUser.ID)
					}
					if user.Username != realUser.Username {
						t.Error("user.Username != realUser.Username", user.Username, realUser.Username)
					}
				})
				foundKeys["users"] = true
			}
		case "finished_at":
			return nil
		default:
			t.Error("unexpected key", string(key))
		}
		return nil
	})

	for _, key := range []string{"id", "users"} {
		if _, exists := foundKeys[key]; !exists {
			t.Error("has no key", key)
		}
	}
}
