package models

import (
	"encoding/json"
	"strconv"
	"testing"

	"github.com/buger/jsonparser"

	"gitlab.com/pipocavsobake/shakal/lib/utils"
)

func createFakeUser(pw string) User {
	user := User{Username: "11111", Password: &pw}
	return user
}

func TestUserJson(t *testing.T) {
	user := createFakeUser("12345")
	user.ID = 1
	bs, err := json.Marshal(user)
	utils.PErr(err)
	var usernameOk, idOk bool

	jsonparser.ObjectEach(bs, func(key []byte, value []byte, dataType jsonparser.ValueType, offset int) error {
		switch string(key) {
		case "id":
			if id, err := strconv.ParseInt(string(value), 10, 64); err != nil {
				utils.PErr(err)
			} else {
				if id != user.ID {
					t.Error("id != User.ID", id, user.ID)
				} else {
					idOk = true
				}
			}

		case "username":
			if username := string(value); username != user.Username {
				t.Error("username != User.Username", username, user.Username)
			} else {
				usernameOk = true
			}
		default:
			t.Error("unexpected key", string(key))
		}
		return nil
	})

	if !idOk {
		t.Error("has not key id")
	}

	if !usernameOk {
		t.Error("has not key username")
	}
}
