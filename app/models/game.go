package models

import (
	"encoding/json"
	"errors"
	"log"
	"sort"
	"time"

	"gitlab.com/pipocavsobake/shakal/kv/abstract"
	"gitlab.com/pipocavsobake/shakal/kv/key"
	libgame "gitlab.com/pipocavsobake/shakal/lib/game"
	"gitlab.com/pipocavsobake/shakal/lib/utils"
)

type Game struct {
	ID         int64      `gorm:"primary_key" json:"id"`
	Users      []*User    `gorm:"many2many:user_games;" json:"users"`
	FinishedAt *time.Time `json:"finished_at"`
}

func (game *Game) KVKey() []byte {
	if game.ID == 0 {
		panic("game.ID == 0")
	}
	return key.For("Game", game.ID)
}

func (game *Game) PlayerIDs() (ret []int64) {
	ret = []int64{}
	if game.Users != nil {
		for _, u := range game.Users {
			ret = append(ret, u.ID)
		}
	} else {
		err := DB.Model("UserGame").Where("game_id = ?", game.ID).Pluck("user_id", &ret)
		if err != nil {
			panic(err)
		}
	}
	return
}

func (game *Game) KVGet(engine abstract.Engine) (gameData *libgame.Game) {
	raw, err := engine.Get(game.KVKey())
	if err != nil {
		log.Println(err.Error())
		gameData = libgame.NewGame()
		gameData.Players = game.PlayerIDs()
		raw, err = json.Marshal(gameData)
		utils.PErr(err)
		err = engine.Set(game.KVKey(), raw)
		utils.PErr(err)
		return
	}
	gameData = &libgame.Game{}
	err = json.Unmarshal(raw, gameData)
	utils.PErr(err)
	return
}

func (game *Game) KVSet(engine abstract.Engine, gameData *libgame.Game) error {
	data, err := json.Marshal(gameData)
	if err != nil {
		return err
	}
	return engine.Set(game.KVKey(), data)
}

func (game *Game) MyPlayer(cu *User) (uint, error) {
	userIDs := make([]int64, len(game.Users))
	//spew.Dump(game.Users)
	for i, user := range game.Users {
		userIDs[i] = user.ID
	}
	sort.Slice(userIDs, func(i, j int) bool { return userIDs[i] < userIDs[j] })
	for i := 0; i < len(userIDs); i++ {
		if userIDs[i] == cu.ID {
			return uint(i), nil
		}
	}
	return uint(0), errors.New("no my player")
}
