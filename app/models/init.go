package models

import (
	"gitlab.com/pipocavsobake/shakal/config"

	"gorm.io/gorm"
)

var DB *gorm.DB

func init() {
	var err error
	DB = config.DB

	err = DB.SetupJoinTable(&User{}, "Games", &UserGame{})
	if err != nil {
		panic(err)
	}
	err = DB.AutoMigrate(&Game{}, &User{}, &UserGame{})
	if err != nil {
		panic(err)
	}
	//DB.Model(&UserGame{}).AddUniqueIndex("user_id", "game_id")
}
