package models

import (
	"testing"
)

func TestUserGameForeignKeys(t *testing.T) {
	sql := "select constraint_name from information_schema.table_constraints where constraint_type = 'FOREIGN KEY'"
	foundNames := make(map[string]bool)
	rows, err := DB.Raw(sql).Rows()
	if err != nil {
		t.Error(err.Error())
	}
	defer rows.Close()
	for rows.Next() {
		var name string
		rows.Scan(&name)
		foundNames[name] = true
	}

}
