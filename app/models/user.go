package models

import "gorm.io/gorm"

type User struct {
	ID                int64   `gorm:"primary_key" json:"id"`
	Username          string  `gorm:"not null;unique_index" json:"username"`
	Password          *string `gorm:"-" json:"-"`
	EncryptedPassword string  `gorm:"not null" json:"-"`
	Games             []Game  `gorm:"many2many:user_games;" json:"-"`
}

func (user *User) GetPID() string {
	return user.Username
}

func (user *User) PutPID(pid string) {
	user.Username = pid
}

func (user *User) GetPassword() string {

	//spew.Dump(user.EncryptedPassword)
	return user.EncryptedPassword
}

func (user *User) PutPassword(password string) {
	//spew.Dump(password)
	user.EncryptedPassword = password
}

func FindUser(username string) (user User, err error) {
	if err = DB.Where("username = ?", username).First(&user).Error; err != nil {
		return
	}
	if user.ID == 0 {
		err = gorm.ErrRecordNotFound
		return
	}
	return
}
