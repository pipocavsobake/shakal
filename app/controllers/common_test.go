package controllers

import (
	"github.com/gin-gonic/gin"
	webpack "github.com/go-webpack/webpack"
	"gitlab.com/pipocavsobake/shakal/config"
)

func getRouter() *gin.Engine {
	gin.SetMode(gin.TestMode)
	r := gin.Default()
	r.HTMLRender = config.Render

	webpack.DevHost = "localhost:7310"
	webpack.Plugin = "manifest"
	webpack.Verbose = false
	webpack.FsPath = "../../public/webpack"
	webpack.Init(false)

	return r
}
