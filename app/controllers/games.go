package controllers

import (
	"encoding/json"
	"log"
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/pipocavsobake/shakal/app/channels"
	"gitlab.com/pipocavsobake/shakal/app/models"
	"gitlab.com/pipocavsobake/shakal/config/auth"
	"gitlab.com/pipocavsobake/shakal/kv/abstract"
	"gitlab.com/pipocavsobake/shakal/lib/game"
)

type GameParams struct {
	UserIds []int64 `json:"users"`
}

func CreateGame(ctx *gin.Context) {
	var params GameParams
	if err := ctx.ShouldBindJSON(&params); err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, ApiError{err.Error()})
		return
	}
	game := models.Game{}
	if currentUser, err := auth.AuthBoss.CurrentUser(ctx.Request); err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, ApiError{err.Error()})
		return
	} else {
		params.UserIds = append(params.UserIds, currentUser.(*models.User).ID)
	}
	tx := models.DB.Begin()
	if err := tx.Create(&game).Error; err != nil {
		tx.Rollback()
		ctx.JSON(http.StatusUnprocessableEntity, ApiError{err.Error()})
		return
	}
	for _, userId := range params.UserIds {
		ug := models.UserGame{UserID: &userId, GameID: &game.ID}
		if err := tx.Save(&ug).Error; err != nil {
			tx.Rollback()
			ctx.JSON(http.StatusUnprocessableEntity, ApiError{err.Error()})
			return
		}
	}

	if err := tx.Commit().Error; err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, ApiError{err.Error()})
		return
	}

	if err := models.DB.Model("Game").Preload("Users").Where("id = ?", game.ID).First(&game).Error; err != nil {
		ctx.JSON(http.StatusUnprocessableEntity, ApiError{err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, game)
}

type GamesIndexParams struct {
	Page int `form:"page"`
}

type GamesIndexResult struct {
	Total int64         `json:"total"`
	Data  []models.Game `json:"data"`
	Page  int           `json:"page"`
}

func GamesIndex(ctx *gin.Context) {
	var params GamesIndexParams
	var result GamesIndexResult
	if params.Page <= 0 {
		params.Page = 1
	}
	result.Page = params.Page
	if err := ctx.ShouldBindQuery(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error()})
		return
	}
	scope := models.DB.Model(&models.Game{}).Preload("Users")
	if err := scope.Count(&result.Total).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error()})
		return
	}
	if err := scope.Limit(20).Offset((params.Page - 1) * 20).Find(&result.Data).Error; err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, result)
}

type Games struct {
	Engine abstract.Engine
}

type GameShowParams struct {
	ID int64 `uri:"id" binding:"required"`
}

type GameShowResult struct {
	Game models.Game    `json:"game"`
	Data *game.Extended `json:"data"`
}

func (games *Games) Show(ctx *gin.Context) {
	var params GameShowParams
	var result GameShowResult
	if err := ctx.ShouldBindUri(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error()})
		return
	}
	var g models.Game
	if err := models.DB.Model(&g).Preload("Users").Where("id = ?", params.ID).First(&result.Game).Error; err != nil {
		ctx.JSON(http.StatusNotFound, ApiError{err.Error()})
		return
	}
	gm := result.Game.KVGet(games.Engine)
	ex := gm.Extend()
	result.Data = &ex

	if currentUser, err := auth.AuthBoss.CurrentUser(ctx.Request); err == nil {
		result.Data.MyPlayer, _ = result.Game.MyPlayer(currentUser.(*models.User))
	}
	ctx.JSON(http.StatusOK, result)
}

func (games *Games) Update(ctx *gin.Context) {
	var showParams GameShowParams
	if err := ctx.ShouldBindUri(&showParams); err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error()})
		return
	}
	var params game.HumanAction
	if err := ctx.ShouldBindJSON(&params); err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error()})
		return
	}
	action, err := params.Action()
	if err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error()})
		return
	}
	cu := &models.User{}
	if currentUser, err := auth.AuthBoss.CurrentUser(ctx.Request); err != nil {
		ctx.JSON(http.StatusUnauthorized, ApiError{err.Error()})
		return
	} else {
		cu = currentUser.(*models.User)
	}
	var result GameShowResult
	var g models.Game
	if err := models.DB.Model(&g).Preload("Users").Where("id = ?", showParams.ID).First(&result.Game).Error; err != nil {
		ctx.JSON(http.StatusNotFound, ApiError{err.Error()})
		return
	}
	gm := result.Game.KVGet(games.Engine)
	ngm := game.Reduce(gm, action)
	ex := ngm.Extend()

	result.Data = &ex
	if result.Data.MyPlayer, err = result.Game.MyPlayer(cu); err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error()})
		return
	}
	if err = result.Game.KVSet(games.Engine, ngm); err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error()})
		return
	}

	ctx.JSON(http.StatusOK, result)
	data, err := json.Marshal(result.Data)
	if err != nil {
		log.Println(err.Error())
	}
	channels.Broadcast(result.Game.ID, cu.ID, data)
}
