package controllers

import (
	"github.com/gin-gonic/gin"

	"net/http"
)

func HomeIndex(ctx *gin.Context) {
	ctx.Status(http.StatusOK)
	Render.Layout("base")
	Render.Execute("home/index", gin.H{}, ctx.Request, ctx.Writer)

}
