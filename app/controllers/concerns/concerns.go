package concerns

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/pipocavsobake/shakal/app/models"
	"gitlab.com/pipocavsobake/shakal/config"
)

type ApiError struct {
	Error string `json:"error"`
}

func CurrentUser(ctx *gin.Context) (user *models.User, err error) {
	session, _ := config.SessionStore.Get(ctx.Request, "current_user")
	userId, ok := session.Values["user_id"]
	if !ok {
		return
	}
	u := models.User{}
	if err = models.DB.Where("id = ?", userId.(int64)).First(&u).Error; err != nil {
		return
	}
	user = &u
	return
}
