package controllers

import (
	"html/template"
	"net/http"
	"os"

	webpack "github.com/go-webpack/webpack"
	"github.com/qor/render"
	"gitlab.com/pipocavsobake/shakal/assets"
)

var Render *render.Render

type ApiError struct {
	Error string `json:"error"`
}

func init() {
	viewPath := os.Getenv("VIEW_PATH")
	if viewPath == "" {
		viewPath = "app/views"
	}
	Render = render.New(&render.Config{
		ViewPaths:     []string{viewPath},
		DefaultLayout: "base",
		FuncMapMaker: func(*render.Render, *http.Request, http.ResponseWriter) template.FuncMap {
			return template.FuncMap{
				"asset": webpack.AssetHelper,
				//"json":  json.Marshal,
			}
		},
	})

	Render.SetAssetFS(assets.AssetFS)
}
