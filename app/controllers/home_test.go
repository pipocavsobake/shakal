package controllers

import (
	"net/http"
	"net/http/httptest"

	"github.com/PuerkitoBio/goquery"

	"gitlab.com/pipocavsobake/shakal/lib/utils"

	"testing"
)

func TestHomeIndex(t *testing.T) {
	r := getRouter()
	//r.LoadHTMLFiles("../views/home/index.tmpl")
	r.GET("/", HomeIndex)
	req, err := http.NewRequest(http.MethodGet, "/", nil)
	if err != nil {
		t.Fatalf("Couldn't create request: %v\n", err)
	}
	w := httptest.NewRecorder()
	r.ServeHTTP(w, req)
	if w.Code != http.StatusOK {
		t.Fatalf("Expected to get status %d but instead got %d\n", http.StatusOK, w.Code)
	}
	doc, err := goquery.NewDocumentFromReader(w.Body)
	utils.PErr(err)
	if doc.Find("#root").Length() == 0 {
		t.Error("response body has no #root", w.Body.String())
	}
	if doc.Find("script").Length() == 0 {
		html, err := doc.Html()
		utils.PErr(err)
		t.Error("response body has no script tag; it seems to be absence of layout", html)
	}
}
