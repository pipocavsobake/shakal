package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/pipocavsobake/shakal/app/models"
	"gitlab.com/pipocavsobake/shakal/config/auth"
)

type UserValuer struct {
	Username string `json:"username" validate:"required"`
	Password string `json:"password" validate:"required"`
}

func (uv *UserValuer) GetPID() string {
	return uv.Username
}
func (uv *UserValuer) GetPassword() string {
	return uv.Password
}
func (uv *UserValuer) Validate() []error {
	return []error{}
}

type UserSearchParams struct {
	Q string `form:"q"`
}

type UserSearchResult struct {
	Total int64         `json:"total"`
	Data  []models.User `json:"data"`
}

func UserSearch(ctx *gin.Context) {
	var params UserSearchParams
	var result UserSearchResult
	ctx.ShouldBindQuery(&params)
	// TODO tsvector
	scope := models.DB.Model(&models.User{}).Where("username ILIKE ?", "%"+params.Q+"%")
	if err := scope.Count(&result.Total).Error; err != nil {
		panic(err)
	}
	if err := scope.Find(&result.Data).Limit(20).Error; err != nil {
		panic(err)
	}
	ctx.JSON(200, result)
}

func UsersMe(ctx *gin.Context) {
	currentUser, err := auth.AuthBoss.CurrentUser(ctx.Request)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, ApiError{err.Error()})
		return
	}
	ctx.JSON(http.StatusOK, currentUser.(*models.User))
}
