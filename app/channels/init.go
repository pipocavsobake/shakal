package channels

import (
	"log"
	"strconv"
	"strings"

	"github.com/davecgh/go-spew/spew"
	"gitlab.com/pipocavsobake/shakal/app/models"
	"gitlab.com/pipocavsobake/shakal/config/auth"
	melody "gopkg.in/olahol/melody.v1"
)

var m *melody.Melody

const USER_ID_KEY = "user_id"
const GAME_ID_KEY = "game_id"

func Handler() *melody.Melody {
	if m == nil {
		m = melody.New()
		m.HandleConnect(func(session *melody.Session) {
			cu, err := auth.AuthBoss.CurrentUser(session.Request)
			if err != nil {
				log.Println(err.Error())
			} else {
				if session.Keys == nil {
					session.Keys = make(map[string]interface{})
				}
				path := session.Request.URL.Path
				parts := strings.Split(path, "/")
				gameIDStr := parts[len(parts)-1]
				if gameID, err := strconv.ParseInt(gameIDStr, 10, 64); err != nil {
					spew.Dump(err)
				} else {
					session.Keys[GAME_ID_KEY] = gameID
				}

				session.Keys[USER_ID_KEY] = cu.(*models.User).ID
			}
			//spew.Dump(cu)
			//spew.Dump(session.Keys)
		})
	}
	return m
}

func Broadcast(gameID int64, userID int64, msg []byte) {
	println("broadcast")
	//spew.Dump(m.Len())
	//spew.Dump(gameID, userID)
	m.BroadcastFilter(msg, func(session *melody.Session) bool {
		//spew.Dump(session.Keys)
		if session.Keys == nil {
			println("nil")
			return false
		}
		if uID, ok := session.Keys[USER_ID_KEY]; !ok || uID.(int64) == userID {
			println("user")
			return false
		}
		if gID, ok := session.Keys[GAME_ID_KEY]; !ok || gID.(int64) != gameID {
			println("game")
			return false
		}
		//spew.Dump(session)
		return true
	})
}
