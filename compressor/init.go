package compressor

import (
	"bytes"
	"compress/gzip"
	"io/ioutil"

	"github.com/pkg/errors"
	"gitlab.com/pipocavsobake/shakal/kv/abstract"
)

func Compress(e abstract.Engine) Engine {
	return Engine{e: e}
}

type Engine struct {
	e abstract.Engine
}

func (e *Engine) Get(key []byte) ([]byte, error) {
	bts, err := e.e.Get(key)
	if err != nil {
		return bts, errors.Wrap(err, "comressor GetBytes delegate")
	}
	r, err := gzip.NewReader(bytes.NewReader(bts))
	if err != nil {
		return bts, errors.Wrap(err, "comressor GetBytes gunzip")
	}
	ret, err := ioutil.ReadAll(r)
	if err != nil {
		return []byte{}, errors.Wrap(err, "comressor GetBytes readall")
	}
	return ret, nil
}

func (e *Engine) Set(key []byte, body []byte) error {
	var b bytes.Buffer
	gz := gzip.NewWriter(&b)
	if _, err := gz.Write(body); err != nil {
		return errors.Wrap(err, "compressor SetBytes gz.Write")
	}
	if err := gz.Flush(); err != nil {
		return errors.Wrap(err, "compressor SetBytes gz.Flush")

	}
	if err := gz.Close(); err != nil {
		return errors.Wrap(err, "compressor SetBytes gz.Close")
	}
	if err := e.e.Set(key, b.Bytes()); err != nil {
		return errors.Wrap(err, "compressor SetBytes")
	}
	return nil

}

func (e *Engine) Delete(key []byte) error {
	return e.e.Delete(key)
}
