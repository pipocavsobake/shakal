package routes

import (
	"net/http"

	"github.com/gin-gonic/gin"
	adapter "github.com/gwatts/gin-adapter"

	"gitlab.com/pipocavsobake/shakal/app/channels"
	"gitlab.com/pipocavsobake/shakal/app/controllers"
	"gitlab.com/pipocavsobake/shakal/config"
	"gitlab.com/pipocavsobake/shakal/config/auth"
	"gitlab.com/pipocavsobake/shakal/kv/abstract"
)

var r *gin.Engine

func Router(e abstract.Engine) *gin.Engine {
	if r == nil {
		r = gin.Default()
		r.GET("/", controllers.HomeIndex)
		m := channels.Handler()
		r.Use(adapter.Wrap(auth.AuthBoss.LoadClientStateMiddleware))
		r.Any("/ws/:id", func(c *gin.Context) {
			m.HandleRequest(c.Writer, c.Request)
		})
		au := r.Group("/auth")
		au.Any("/*w",
			gin.WrapH(
				auth.AuthBoss.LoadClientStateMiddleware(
					http.StripPrefix("/auth", auth.AuthBoss.Config.Core.Router)),
			),
		)
		games := controllers.Games{Engine: e}
		r.GET("/users/search", controllers.UserSearch)
		api := r.Group("/api")
		api.GET("/users/me", controllers.UsersMe)
		api.GET("/games", controllers.GamesIndex)
		api.GET("/games/:id", games.Show)
		api.PUT("/games/:id", games.Update)
		api.POST("/games", controllers.CreateGame)
		r.NoRoute(controllers.HomeIndex)
		r.HTMLRender = config.Render
	}

	for _, path := range []string{"system", "webpack", "images"} {
		r.Static("/"+path, "public/"+path)
	}
	return r
}
