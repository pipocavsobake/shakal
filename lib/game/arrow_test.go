package game

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestArrowMoveTo(t *testing.T) {
	id := uint(50)
	for _, level := range []uint{1, 2, 4, 8} {
		arrow := NewArrow(id, level, 0)
		assert.Len(t, arrow.CanMoveTo(), int(level))
		if level == 1 {
			assert.Equal(t, arrow.MustMoveTo(), uint(37))
		} else {
			assert.Panics(t, func() { arrow.MustMoveTo() })
		}
	}
}

func TestArrowDirection(t *testing.T) {
	id := uint(50)
	cells := make(map[uint]struct{})
	for direction := uint(0); direction < 8; direction++ {
		cell := NewArrow(id, 1, direction).MustMoveTo()
		assert.NotContains(t, cells, cell)
		cells[cell] = struct{}{}
	}
}
