package game

import (
	"errors"
)

type Extended struct {
	Desk          []HumanCell `json:"desk"`
	MyPlayer      uint        `json:"my_player"`
	CurrentPlayer uint        `json:"current_player"`
	Results       [4]uint     `json:"results"`
	Moves         []Move      `json:"moves"`
	Selected      []uint      `json:"selected"`
	CurrentCell   *uint       `json:"current_cell"`
}

type HumanCell struct {
	ID         uint        `json:"id"`
	Type       string      `json:"type"`
	Achievable bool        `json:"achievable"`
	Direction  *uint       `json:"direction"`
	Level      *uint       `json:"level"`
	Owner      *uint       `json:"owner"`
	Chips      []HumanChip `json:"chips"`
}

type HumanChip struct {
	ID         uint   `json:"id"`
	Type       string `json:"type"`
	Selected   bool   `json:"selected"`
	Selectable bool   `json:"selectable"`
	Owner      *uint  `json:"owner"`
	Level      *uint  `json:"level"`
}

var HUMAN_TYPES = []string{
	"closed",
	"marsh",
	"treasure",
	"arrow",
	"star",
	"hedgehog",
	"empty",
	"horse",
	"shark",
	"donkey",
	"friday",
	"vortex",
	"block",
	"island",
	"water",
}

func (cell *Cell) HumanType() string {
	if !cell.Opened {
		return "closed"
	}
	return HUMAN_TYPES[cell.Type]
}

var CHIP_HUMAN_TYPES = []string{
	"",
	"ship",
	"chip",
	"coin",
	"friday",
	"donkey",
}

func (chip *Chip) HumanType() string {
	return CHIP_HUMAN_TYPES[chip.Type]
}

func NewExtended(game *Game) (e Extended) {
	tr := true
	stype := SHIP
	ctype := COIN_CHIP
	dtype := DONKEY_CHIP
	selected := game.FilterChips(ChipFilter{Selected: &tr})
	coins := game.FilterChips(ChipFilter{Selected: &tr, Type: &ctype})
	donkeySelected := len(game.FilterChips(ChipFilter{Selected: &tr, Type: &dtype})) > 0
	sc := len(coins) > 0
	mustContinue := len(selected) > 0 && (game.Desk[selected[0].Cell].Type == ARROW || game.Desk[selected[0].Cell].Type == HORSE)
	ship := game.FilterChips(ChipFilter{Owner: &game.Player, Type: &stype})[0]
	ships := game.ChipsOfType(SHIP)
	moves := []Move{}
	if len(selected) > 0 {
		moves = game.Moves(*selected[0], ship)
	}
	e.CurrentPlayer = game.Player
	e.Moves = moves
	e.Desk = make([]HumanCell, len(game.Desk))
	e.Selected = make([]uint, 0)
	for id, cell := range game.Desk {
		achievable := false
		for _, move := range moves {
			if move.To == cell.ID {
				achievable = true
			}
		}
		if !cell.Opened {
			e.Desk[id] = HumanCell{
				ID:         cell.ID,
				Type:       "closed",
				Chips:      make([]HumanChip, 0),
				Achievable: achievable,
			}
			continue
		}
		chips := game.FilterChips(ChipFilter{Cell: &cell.ID})
		hchips := make([]HumanChip, 0)
		for _, chip := range chips {
			if chip.Sinked {
				continue
			}
			if chip.Selected {
				e.Selected = append(e.Selected, chip.ID)
				ccid := cell.ID
				e.CurrentCell = &ccid
			}
			sel1 := (chip.Owner != nil && *chip.Owner == game.Player && // данная фигура моя
				(!chip.Selected || !sc) && // (либо она не выбрана, либо нет выбранных монет)
				chip.Type != COIN_CHIP) // данная фигура не монета
			sel2 := (chip.Type == COIN_CHIP && // данная фигура монета
				chip.Cell != ships[0].Cell && // данная монета не стоит на корабле
				chip.Cell != ships[1].Cell && // данная монета не стоит на корабле
				chip.Cell != ships[2].Cell && // данная монета не стоит на корабле
				chip.Cell != ships[3].Cell && // данная монета не стоит на корабле
				len(selected) > 0 && // что-то уже выбрано
				chip.Cell == selected[0].Cell && // данная монета на той же клетке
				(game.Desk[chip.Cell].Type != MARSH || // либо клетка не болото
					chip.Level == selected[0].Level) && // либо монета на том же уровне болота
				(chip.Selected || !sc || (donkeySelected && len(coins) == 1))) // (либо данная монета не выбрана, либо нет выбранных монет либо выбрана одна + ишак)
			//if chip.ID == 18 {
			//println(18)
			//spew.Dump(chip, cell, selected, coins, game.Desk[chip.Cell].Type, game.Player, sel1, sel2)
			//}
			hchip := HumanChip{
				ID:       chip.ID,
				Type:     chip.HumanType(),
				Selected: chip.Selected,
				Selectable: !mustContinue &&
					(sel1 ||
						sel2),
			}
			if chip.Type != COIN_CHIP {
				hchip.Owner = chip.Owner
			}
			if cell.Type == MARSH {
				//spew.Dump(chip)
				lvl := chip.Level
				hchip.Level = &lvl
			}
			hchips = append(hchips, hchip)
		}
		e.Desk[id] = HumanCell{
			ID:         cell.ID,
			Type:       cell.HumanType(),
			Chips:      hchips,
			Achievable: achievable,
		}
		if cell.Type == STAR || cell.Type == ARROW {
			dir := cell.Direction
			e.Desk[id].Direction = &dir
		}
		if cell.Type == MARSH || cell.Type == ARROW || cell.Type == TREASURE || cell.Type == BLOCK {
			lvl := cell.Level
			e.Desk[id].Level = &lvl
		}
	}
	e.Results = game.Results()
	return
}

func (game *Game) Extend() Extended {
	return NewExtended(game)
}

type HumanAction struct {
	Type    string `json:"type"`
	ID      uint   `json:"id"`
	ChipIDs []uint `json:"chip_ids"`
	To      uint   `json:"to"`
}

func (params *HumanAction) Action() (Action, error) {
	var tp ActionType
	switch params.Type {
	case "move":
		tp = MOVE
	case "select":
		tp = SELECT
	default:
		return Action{}, errors.New("unexpected action type")
	}
	return Action{Type: tp, ID: params.ID, ChipIDs: params.ChipIDs, To: params.To}, nil
}
