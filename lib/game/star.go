package game

type Star struct {
	Direction, ID uint
}

func NewStar(direction uint, id uint) *Star {
	return &Star{Direction: direction, ID: id}
}

func (star *Star) MustMoveTo() uint {
	r := star.ID % 13
	rs := []uint{r, star.ID - r + 12, r + 13*12, star.ID - r}
	return rs[star.Direction]
}

func (cell *Cell) Star() *Star {
	return NewStar(cell.Direction, cell.ID)
}
