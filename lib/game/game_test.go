package game

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestValidator(t *testing.T) {
	t.Log("should mark desk with 1-arrow cycle as invalid")
	prepare()
	directions := map[uint]uint{
		15: 2,
		16: 4,
		29: 6,
		28: 0,
	}
	for i, cell := range dsk {
		if direction, ok := directions[cell.ID]; ok {
			dsk[i] = Cell{Type: ARROW, ID: cell.ID, Direction: direction, Level: 1}
		}
	}
	game.Desk = dsk
	assert.False(t, game.IsDeskValid())
}

func TestNewGame(t *testing.T) {
	t.Log("New game should have valid desk")
	game := NewGame()
	assert.Nil(t, game.Validate())
}

func TestFilterCells(t *testing.T) {
	t.Log("filter should filter by type")
	prepare()
	id := uint(40)
	cells := game.FilterCells(CellFilter{ID: &id})
	assert.Len(t, cells, 1)
	assert.NotContains(t, cells, nil)
	assert.Equal(t, cells[0].ID, id)

	t.Log("filter should respec Copy parameter")
	cells = game.FilterCells(CellFilter{Copy: true})
	assert.NotContains(t, cells, &game.Desk[0])
	cells = game.FilterCells(CellFilter{Copy: false})
	assert.Contains(t, cells, &game.Desk[0])

	t.Log("filter should combine props")
	arrowType := ARROW
	level := uint(4)
	cells = game.FilterCells(CellFilter{Type: &arrowType, Level: &level})
	assert.NotContains(t, cells, nil)
	for _, cell := range cells {
		assert.Equal(t, cell.Type, ARROW)
		assert.Equal(t, cell.Level, level)
	}

	t.Log("filter should use func")
	ff := func(cell Cell) bool {
		return cell.ID > 2 && cell.ID < 10
	}
	cells = game.FilterCells(CellFilter{Func: &ff, Copy: false})
	assert.NotContains(t, cells, nil)
	for i, cell := range game.Desk {
		if ff(cell) {
			assert.Contains(t, cells, &game.Desk[i])
		} else {
			assert.NotContains(t, cells, &game.Desk[i])
		}
	}
}
