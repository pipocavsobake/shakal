package game

import (
	"gitlab.com/pipocavsobake/shakal/lib/utils"
)

type ActionType uint

const (
	SELECT ActionType = iota + 1
	MOVE
)

type Action struct {
	To      uint       `json:"to"`
	ChipIDs []uint     `json:"chip_ids"`
	Level   uint       `json:"level"`
	Type    ActionType `json:"type"`
	ID      uint       `json:"id"`
}

type Reducer struct {
	game   Game
	action Action
	toCell Cell
}

func NewReducer(game Game, action Action) Reducer {
	r := Reducer{game: game, action: action}
	if action.Type != MOVE {
		return r
	}
	r.toCell = game.Desk[action.To]
	if r.toCell.ID != action.To {
		panic("r.toCell.ID != action.To")
	}
	if r.toCell.Type == MARSH && r.game.Chips[r.action.ChipIDs[0]].Cell != r.action.To {
		r.action.Level = 1
	}
	if r.toCell.Type == MARSH && r.game.Chips[r.action.ChipIDs[0]].Cell == r.action.To {
		r.action.Level = r.game.Chips[r.action.ChipIDs[0]].Level + 1
	}
	return r
}

func (r *Reducer) knockOut(chip Chip, id uint) Chip {
	if !r.toCell.Opened || chip.Type == COIN_CHIP || chip.Cell != r.action.To || chip.Level != r.action.Level || (chip.Owner != nil && *chip.Owner == r.game.Player) || r.toCell.Type == WATER {
		return chip
	}
	chip.Cell = r.getHome(chip)
	chip.Level = 0
	return chip
}

// найти id домашней клетки для данного chip
func (r *Reducer) getHome(chip Chip) uint {
	switch chip.Type {
	case CHIP:
		stype := SHIP
		return r.game.FilterChips(ChipFilter{Type: &stype, Owner: chip.Owner})[0].Cell
	case FRIDAY_CHIP:
		return r.game.CellsOfType(FRIDAY)[0].ID
	case DONKEY_CHIP:
		return r.game.CellsOfType(DONKEY)[0].ID
	default:
		panic("unsupported chip.Type")
	}
}

// сделать chip видимым (ишак или пятница)
func (r *Reducer) openChip(chip Chip, id uint) Chip {
	if r.toCell.Opened || chip.Cell != r.action.To || chip.Opened {
		return chip
	}
	chip.Opened = true
	player := r.game.Player
	if chip.Type != COIN_CHIP {
		chip.Owner = &player
	}
	return chip
}

// тотопить монеты, съедать акулой
func (r *Reducer) wreck(chip Chip, id uint) Chip {
	here := utils.UintIncludes(r.action.ChipIDs, id)
	ship := r.game.Chips[4*r.game.Player]
	isShipMoving := utils.UintIncludes(r.action.ChipIDs, ship.ID)
	coinToSink := !r.toCell.Type.SafeForCoins() && (chip.Type == COIN_CHIP || chip.Type == DONKEY_CHIP) && ship.Cell != r.action.To && !isShipMoving
	onShark := r.toCell.Type == SHARK

	if here && (coinToSink || onShark) {
		chip.Sinked = true
	}
	return chip
}

// перенести объекты
func (r *Reducer) move(chip Chip, id uint) Chip {
	if !utils.UintIncludes(r.action.ChipIDs, id) {
		return chip
	}
	level := r.action.Level
	if level == 0 && r.toCell.Type == MARSH {
		level = 1
	}
	chip.Level = level
	chip.Cell = r.action.To
	return chip
}

// Собственно reduce
func (r *Reducer) newChips() (ret []Chip) {
	ret = make([]Chip, len(r.game.Chips))
	for id, chip := range r.game.Chips {

		chip = r.knockOut(chip, uint(id))
		chip = r.openChip(chip, uint(id))
		chip = r.wreck(chip, uint(id))
		chip = r.move(chip, uint(id))

		ret[id] = chip
	}
	return
}

// активация пиратского корабля или осьминога (block)
// могут быть две причины:
// 1. Все фишки (chips) его покинули
// 2. Игрок вышибает другого
func (r *Reducer) activateBlock(cell Cell, id uint, myChipsHere []*Chip) Cell {
	if cell.Type != BLOCK || cell.Active == true {
		return cell
	}
	cond1 := id == r.game.Chips[r.action.ChipIDs[0]].Cell && len(myChipsHere) == len(r.action.ChipIDs)

	fun := func(chip Chip) bool {
		return chip.Type != COIN_CHIP && (chip.Owner == nil || *chip.Owner != r.game.Player)
	}
	cond2 := r.action.To == id && len(r.game.FilterChips(ChipFilter{
		Cell: &id,
		Func: &fun,
	})) > 0

	if cond1 || cond2 {
		cell.Active = true
	}
	return cell
}

// открыть клетку
func (r *Reducer) open(cell Cell, id uint, myChipsHere []*Chip) Cell {
	if id != r.action.To || cell.Opened {
		return cell
	}
	cell.Opened = true
	cell.Owner = &r.game.Player
	return cell
}

// спасение заблокированной фишки (chip) путём прихода подмоги, но не вышибания
func (r *Reducer) deactivateBlock(cell Cell, id uint, myChipsHere []*Chip) Cell {
	if id != r.action.To || cell.Type != BLOCK || uint(len(myChipsHere)+1) < cell.Level {
		return cell
	}
	cell.Active = false
	return cell
}

// combine reducers
func (r *Reducer) newCell(cell Cell, id uint) Cell {
	fun := func(chip Chip) bool {
		return chip.Type != COIN_CHIP
	}
	myChipsHere := r.game.FilterChips(ChipFilter{Cell: &id, Owner: &r.game.Player, Func: &fun})

	cell = r.activateBlock(cell, id, myChipsHere)
	cell = r.open(cell, id, myChipsHere)
	cell = r.deactivateBlock(cell, id, myChipsHere)

	return cell
}

func (r *Reducer) newDesk() (ret []Cell) {
	ret = make([]Cell, len(r.game.Desk))
	for id, cell := range r.game.Desk {
		ret[id] = r.newCell(cell, uint(id))
	}
	return ret
}

func (r *Reducer) nextPlayer() uint {
	ps := uint(4)
	if l := len(r.game.Players); l > 0 {
		ps = uint(l)
	}
	return (r.game.Player + 1) % ps
}

func (r *Reducer) ApplyMove() Game {
	game := Game{Desk: r.newDesk(), Chips: r.newChips(), Player: r.game.Player, Players: r.game.Players}
	switch r.toCell.Type {
	case STAR:
		to := r.toCell.Star().MustMoveTo()
		red := NewReducer(game, Action{Type: MOVE, To: to, ChipIDs: r.action.ChipIDs})
		return red.ApplyMove()
	case HEDGEHOG:
		ids := utils.UintFilterBy(r.action.ChipIDs, true, func(id uint) bool {
			return r.game.Chips[id].Type != COIN_CHIP
		})
		if len(ids) == 0 {
			panic("len(ids) == 0")
		}
		chip := r.game.Chips[*ids[0]]
		red := NewReducer(game, Action{Type: MOVE, To: r.getHome(chip), ChipIDs: r.action.ChipIDs})
		return red.ApplyMove()
	case HORSE:
		return game
	case VORTEX:
		to := 2*r.action.To - r.game.Chips[r.action.ChipIDs[0]].Cell
		action := Action{Type: MOVE, To: to, ChipIDs: r.action.ChipIDs}
		//spew.Dump(r.action, action)
		//debug.PrintStack()
		red := NewReducer(game, action)
		return red.ApplyMove()
	case ARROW:
		if r.toCell.Level != 1 {
			return game
		}
		to := r.toCell.Arrow().MustMoveTo()
		red := NewReducer(game, Action{Type: MOVE, To: to, ChipIDs: r.action.ChipIDs})
		return red.ApplyMove()
	default:
		game.Player = r.nextPlayer()
		for i, _ := range game.Chips {
			game.Chips[i].Selected = false
		}
		return game
	}
}

func (r *Reducer) ApplySelect() Game {
	target := r.game.Chips[r.action.ID]
	isCoin := target.Type == COIN_CHIP
	isShip := target.Type == SHIP
	chipOnSelectedShip := target.Type == CHIP && target.Owner != nil && r.game.Chips[(*target.Owner)*4].Cell == target.Cell && r.game.Chips[(*target.Owner)*4].Selected
	chips := make([]Chip, len(r.game.Chips))
	for id, chip := range r.game.Chips {
		if isShip && (chip.Owner != nil && *chip.Owner == r.game.Player || chip.Type == COIN_CHIP) && chip.Cell == target.Cell {
			// Случай когда ткнули на корабль
			// Как поступить с фишкой, которая на корабле и принадлежит игроку-владельцу корабля
			chip.Selected = !target.Selected
		} else if uint(id) == r.action.ID {
			chip.Selected = chipOnSelectedShip || !chip.Selected
		} else if !isCoin && chip.Type == COIN_CHIP {
			chip.Selected = false
		} else if isCoin || chip.Type == COIN_CHIP {
		} else {
			chip.Selected = false
		}
		chips[id] = chip
	}
	return Game{Desk: r.game.Desk, Player: r.game.Player, Players: r.game.Players, Chips: chips}
}

func (r *Reducer) Apply() Game {
	switch r.action.Type {
	case SELECT:
		return r.ApplySelect()
	case MOVE:
		return r.ApplyMove()
	default:
		return r.game
	}
}

func Reduce(state *Game, action Action) *Game {
	if action.Type != SELECT && action.Type != MOVE {
		return state
	}
	r := NewReducer(*state, action)
	g := r.Apply()
	return &g
}
