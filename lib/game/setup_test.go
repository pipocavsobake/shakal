package game

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
	"github.com/stretchr/testify/assert"
)

var game Game
var ships []Chip
var chs []Chip
var coins []Chip
var friday Chip
var donkey Chip
var cs []Chip
var dsk []Cell

func prepare() {
	game = Game{Desk: Desk()}
	dsk = game.Desk
	game.Chips = Chips(game.Desk)
	ships = game.ChipsOfType(SHIP)
	chs = game.ChipsOfType(CHIP)
	coins = game.ChipsOfType(COIN_CHIP)
	friday = game.ChipsOfType(FRIDAY_CHIP)[0]
	donkey = game.ChipsOfType(DONKEY_CHIP)[0]
	cs = game.Chips
}

func TestChips1(t *testing.T) {
	prepare()
	t.Log("chips should be an array of chips with types, ids and cells")
	zeroIds := 0
	zeroCells := 0
	for _, chip := range cs {
		assert.NotEqual(t, ChipType(0), chip.Type)
		if chip.Type == ChipType(0) {
			t.Error("chip type cannot be 0")
		}

		if zeroIds > 0 {
			if !assert.NotEqual(t, uint(0), chip.ID) {
				spew.Dump(cs)
			}
		}
		if zeroCells > 0 {
			assert.NotEqual(t, uint(0), chip.Cell)
		}
		if chip.ID == 0 {
			zeroIds += 1
		}
		if chip.Cell == 0 {
			zeroCells += 1
		}
	}
}

func TestChips2(t *testing.T) {
	t.Log("shoud be 4 ships, 12 chips, 35 coins, friday and donkey")
	prepare()
	assert.Len(t, ships, 4)
	assert.Len(t, chs, 12)
	assert.Len(t, coins, 35)
	for _, ship := range ships {
		chips := make([]Chip, 0)
		for _, chip := range chs {
			if chip.Owner != ship.Owner || chip.Cell != ship.Cell {
				continue
			}
			chips = append(chips, chip)
		}
		if !assert.Len(t, chips, 3) {
			spew.Dump(chs)
		}
	}
}

func TestDescSize(t *testing.T) {
	t.Log("should be 13 x 13")
	prepare()
	assert.Len(t, dsk, 13*13)

	waters := game.CellsOfType(WATER)
	t.Log("should have opened water fields")
	for _, water := range waters {
		assert.True(t, water.Opened)
	}
	t.Log("should have 48 water fields")
	assert.Len(t, waters, 48)
}

func TestTreasure(t *testing.T) {
	t.Log("should have number of coins similar to level of treasure")
	prepare()
	treasures := game.CellsOfType(TREASURE)
	coinCounts := make(map[uint]uint)
	for _, coin := range coins {
		if v, ok := coinCounts[coin.Cell]; ok {
			coinCounts[coin.Cell] = v + 1
		} else {
			coinCounts[coin.Cell] = 1
		}
	}
	for _, treasure := range treasures {
		id := treasure.ID
		if !assert.Equal(t, coinCounts[id], treasure.Level) {
			spew.Dump(treasures, coinCounts)
		}
	}
}
