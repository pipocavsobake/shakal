package game

import "fmt"

type Point struct {
	X, Y int
}

func (pt *Point) ToID() uint {
	ret := 13*(pt.Y) + (pt.X)
	if ret < 0 {
		panic(fmt.Sprintf("ret (%v) cannot be < 0", ret))
	}
	return uint(ret)
}

func (pt *Point) Add(pt2 Point) Point {
	return Point{
		X: pt.X + pt2.X, Y: pt.Y + pt2.Y,
	}
}

func NewPoint(id uint) Point {
	return Point{
		X: int(id % 13),
		Y: int((id - id%13) / 13),
	}
}

func (game *Game) validNeighbor(chip Chip, pt Point) bool {
	return pt.X >= 0 && pt.X < 13 && pt.Y >= 0 && pt.Y < 13 &&
		(chip.Type != SHIP || game.Desk[pt.ToID()].Type == WATER)
}

var commonMoves = []Point{{0, -1}, {1, -1}, {1, 0}, {1, 1}, {0, 1}, {-1, 1}, {-1, 0}, {-1, -1}}
var commonNonDiags = []Point{{0, -1}, {1, 0}, {0, 1}, {-1, 0}}
var horseMoves = []Point{{-2, -1}, {-2, 1}, {-1, -2}, {-1, 2}, {1, -2}, {1, 2}, {2, -1}, {2, 1}}

func (game *Game) Digits(chip Chip, ship *Chip) []uint {
	var ids = func(from []Point) (to []uint) {
		to = make([]uint, 0)
		for _, ptFrom := range from {
			pt := ptFrom.Add(NewPoint(chip.Cell))
			if game.validNeighbor(chip, pt) {
				to = append(to, pt.ToID())
			}
		}
		return
	}
	if chip.Type == SHIP {
		ret := []uint{}
		for _, id := range ids(commonNonDiags) {
			if id != 0 && id != 12 && id != 168 && id != 156 {
				ret = append(ret, id)
			}
		}
		return ret
	}
	cell := game.Desk[chip.Cell]
	switch cell.Type {
	case ARROW:
		return cell.Arrow().CanMoveTo()
	case HORSE:
		return ids(horseMoves)
	case WATER:
		if ship != nil && ship.Cell == chip.Cell {
			return ids(commonNonDiags)
		}
		ret := []uint{}
		for _, id := range ids(commonNonDiags) {
			if tp := game.Desk[id].Type; tp == WATER || tp == ISLAND {
				ret = append(ret, id)
			}
		}
		return ret
	case BLOCK:
		if cell.Active {
			return []uint{}
		}
		return ids(commonMoves)
	default:
		return ids(commonMoves)
	}
}

type Move struct {
	To    uint
	Level uint
}

func (game *Game) Moves(chip Chip, ship *Chip) []Move {
	cell := game.Desk[chip.Cell]
	if cell.ID != chip.Cell {
		panic(fmt.Sprintf("cell.ID != chip.Cell %v != %v", cell.ID, chip.Cell))
	}
	if cell.Type == MARSH && chip.Level < cell.Level {
		return []Move{{To: cell.ID, Level: chip.Level + 1}}
	}
	ret := []Move{}
	for _, d := range game.Digits(chip, ship) {
		ret = append(ret, Move{To: d})
	}
	return ret
}
