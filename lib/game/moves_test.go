package game

import (
	"testing"

	"github.com/davecgh/go-spew/spew"
	"github.com/stretchr/testify/assert"
)

func TestNewPoint(t *testing.T) {
	for x := 0; x < 13; x++ {
		for y := 0; y < 13; y++ {
			pt := NewPoint(uint(13*y + x))
			assert.Equal(t, pt.X, x)
			assert.Equal(t, pt.Y, y)
		}
	}
}

func TestMovesForChipsOnShip(t *testing.T) {
	game := NewGame()
	chips := game.ChipsOfType(CHIP)
	ships := map[uint]Chip{}
	for _, ship := range game.ChipsOfType(SHIP) {
		ships[*ship.Owner] = ship
	}
	t.Log("should give 3 options per each chip on ship")
	for _, chip := range chips {
		ship, ok := ships[*chip.Owner]
		if !assert.True(t, ok) {
			spew.Dump(ships)
		}
		moves := game.Moves(chip, &ship)
		assert.Len(t, moves, 3)
	}
	t.Log("should give 2 options per each ship")
	for _, ship := range ships {
		moves := game.Moves(ship, nil)
		assert.Len(t, moves, 2)
	}

	t.Log("should give 8 options for donkey")
	donkey := game.ChipsOfType(DONKEY_CHIP)[0]
	home := game.CellsOfType(DONKEY)[0]
	assert.Equal(t, donkey.Cell, home.ID)
	moves := game.Moves(donkey, nil)
	assert.Len(t, moves, 8)

	t.Log("should give arrow.level options for cell in arrow")
	for _, level := range []uint{2, 4, 8} {
		atype := ARROW
		arrow := game.FilterCells(CellFilter{Level: &level, Type: &atype})[0]
		moves := game.Moves(Chip{Type: CHIP, Cell: arrow.ID}, nil)
		assert.Len(t, moves, int(arrow.Level))
	}

	t.Log("should move through marsh by level")
	for _, marsh := range game.CellsOfType(MARSH) {
		movesFrom := game.Moves(Chip{Type: CHIP, Cell: marsh.ID, Level: marsh.Level}, nil)
		assert.Len(t, movesFrom, 8)
		moves := game.Moves(Chip{Type: CHIP, Cell: marsh.ID, Level: 1}, nil)
		assert.Len(t, moves, 1)
		assert.Equal(t, moves[0].Level, uint(2))
	}

}

func TestWaterMoves(t *testing.T) {
	t.Log("should give 2 options on water without ship and islands")
	game := NewGame()
	for i, cell := range game.Desk {
		if cell.Type == ISLAND {
			game.Desk[i].Type = EMPTY
		}
	}
	moves := game.Moves(Chip{ID: 1, Type: CHIP, Cell: 1}, nil)
	assert.Len(t, moves, 2)

	t.Log("should give 3 options on water without ship, but near island")
	game.Desk[14].Type = ISLAND
	moves = game.Moves(Chip{ID: 1, Type: CHIP, Cell: 1}, nil)
	assert.Len(t, moves, 3)

	t.Log("should not allow move ships to corners")
	ship := Chip{ID: 0, Type: SHIP, Owner: &game.Player, Cell: 1}
	moves = game.Moves(ship, &ship)
	for _, move := range moves {
		assert.NotEqual(t, move.To, uint(0))
	}

	chip := Chip{ID: 1, Type: CHIP, Owner: &game.Player, Cell: 1}
	moves = game.Moves(chip, &ship)
	hasZero := false
	for _, move := range moves {
		if move.To == uint(0) {
			hasZero = true
		}
	}
	assert.True(t, hasZero)
}
