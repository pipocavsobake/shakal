package game

import (
	"errors"
)

type Game struct {
	Desk    []Cell  `json:"desk"`
	Chips   []Chip  `json:"chips"`
	Player  uint    `json:"player"`
	Players []int64 `json:"players"`
}

func (game *Game) ChipsOfType(chipType ChipType) (chips []Chip) {
	chips = make([]Chip, 0)
	for _, chip := range game.Chips {
		if chipType == chip.Type {
			chips = append(chips, chip)
		}
	}
	return
}

func (game *Game) CellsOfType(cellType CellType) (cells []Cell) {
	cells = make([]Cell, 0)
	for _, cell := range game.Desk {
		if cellType == cell.Type {
			cells = append(cells, cell)
		}
	}
	return
}

type CellFilter struct {
	Type         *CellType
	Level        *uint
	Active       *bool
	Opened       *bool
	SafeForCoins *bool
	Direction    *uint
	ID           *uint
	Func         *func(Cell) bool
	Copy         bool
}

func (game *Game) FilterCells(f CellFilter) (cells []*Cell) {
	cells = make([]*Cell, 0)
	for i, cell := range game.Desk {
		if (f.Type == nil || cell.Type == *f.Type) &&
			(f.Level == nil || cell.Level == *f.Level) &&
			(f.Active == nil || cell.Active == *f.Active) &&
			(f.Opened == nil || cell.Opened == *f.Opened) &&
			(f.SafeForCoins == nil || cell.SafeForCoins == *f.SafeForCoins) &&
			(f.Direction == nil || cell.Direction == *f.Direction) &&
			(f.ID == nil || cell.ID == *f.ID) &&
			(f.Func == nil || (*f.Func)(cell)) {
			pt := &game.Desk[i]
			if f.Copy {
				pt = &cell
			}
			cells = append(cells, pt)
		}
	}
	return
}

type ChipFilter struct {
	Type     *ChipType
	Level    *uint
	Opened   *bool
	ID       *uint
	Owner    *uint
	Cell     *uint
	Sinked   *bool
	Selected *bool
	Func     *func(Chip) bool
	Copy     bool
}

func (game *Game) FilterChips(f ChipFilter) (chips []*Chip) {
	chips = make([]*Chip, 0)
	for i, chip := range game.Chips {
		if (f.Type == nil || chip.Type == *f.Type) &&
			(f.Level == nil || chip.Level == *f.Level) &&
			(f.Opened == nil || chip.Opened == *f.Opened) &&
			(f.ID == nil || chip.ID == *f.ID) &&
			(f.Owner == nil || (chip.Owner != nil && *chip.Owner == *f.Owner)) &&
			(f.Cell == nil || chip.Cell == *f.Cell) &&
			(f.Sinked == nil || chip.Sinked == *f.Sinked) &&
			(f.Selected == nil || chip.Selected == *f.Selected) &&
			(f.Func == nil || (*f.Func)(chip)) {
			pt := &game.Chips[i]
			if f.Copy {
				pt = &chip
			}
			chips = append(chips, pt)
		}
	}
	return
}

func (game *Game) IsDeskValid() bool {
	atype := ARROW
	level1 := uint(1)

	step := func(cell Cell) *Cell {
		mmt := cell.Arrow().MustMoveTo()
		cells := game.FilterCells(CellFilter{Type: &atype, Level: &level1, ID: &mmt})
		if len(cells) == 0 {
			return nil
		}
		return cells[0]
	}
	fun := func(cell Cell) bool {
		all := []uint{cell.ID}
		for cur := step(cell); cur != nil; cur = step(*cur) {
			for _, prev := range all {
				if prev == cur.ID {
					return true
				} else {
					all = append(all, cur.ID)
				}
			}
		}
		return false
	}
	arrows2 := game.FilterCells(CellFilter{Type: &atype, Level: &level1, Func: &fun})
	return len(arrows2) == 0
}

func (game *Game) IsCellIdsValid() bool {
	for id, cell := range game.Desk {
		if cell.ID != uint(id) {
			return false
		}
	}
	return true
}
func (game *Game) IsChipIdsValid() bool {
	for id, chip := range game.Chips {
		if chip.ID != uint(id) {
			//spew.Dump(game.Chips)
			return false
		}
	}
	return true
}

func (game *Game) IsChipsHaveOwners() bool {
	ownedChips := map[uint]uint{}
	for _, chip := range game.Chips {
		if chip.Type == CHIP || chip.Type == SHIP {
			if chip.Owner != nil {
				if v, ok := ownedChips[*chip.Owner]; ok {
					ownedChips[*chip.Owner] = v + 1
				} else {
					ownedChips[*chip.Owner] = 1
				}
			} else {
				return false
			}

		}
	}
	owned := 0
	for _, v := range ownedChips {
		if v != uint(4) {
			return false
		}
		owned += 1
	}
	if owned != 4 {
		return false
	}
	return true
}

func (game *Game) Validate() error {
	if !game.IsDeskValid() {
		return errors.New("вечный цикл одиночных стрелок")
	}
	if !game.IsCellIdsValid() {
		return errors.New("wrong cell ids")
	}
	if !game.IsChipIdsValid() {
		return errors.New("wrong chip ids")
	}
	if !game.IsChipsHaveOwners() {
		return errors.New("chips hav not owners")
	}
	return nil
}

func NewGame() (game *Game) {
	game = &Game{}
	attempts := 30
	for attempt := 0; attempt < attempts; attempt++ {
		game.Desk = Desk()
		if game.IsDeskValid() {
			break
		}
	}
	game.Chips = Chips(game.Desk)
	return
}

func (game *Game) Results() (ret [4]uint) {
	ctype := COIN_CHIP
	fa := false
	for i := 0; i < 4; i++ {
		ret[i] = uint(
			len(
				game.FilterChips(
					ChipFilter{
						Type:   &ctype,
						Cell:   &game.Chips[4*i].Cell,
						Sinked: &fa,
					},
				),
			),
		)
	}
	return
}
