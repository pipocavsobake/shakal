package game

import (
	"encoding/json"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/pipocavsobake/shakal/lib/utils"
)

func TestReducerMove(t *testing.T) {
	game := NewGame()
	for i, cell := range game.Desk {
		if cell.Type == ARROW || cell.Type == STAR || cell.Type == HEDGEHOG || cell.Type == VORTEX {
			game.Desk[i].Type = EMPTY
		}
	}
	dump, err := json.Marshal(game)
	utils.PErr(err)
	stype := SHIP
	ships := game.FilterChips(ChipFilter{Type: &stype, Owner: &game.Player})
	if !assert.Len(t, ships, 1) {
		//spew.Dump(game)
		return
	}
	ship := ships[0]

	t.Log("should move chip")
	ctype := CHIP
	chip := game.FilterChips(ChipFilter{Type: &ctype, Owner: &game.Player})[0]
	for _, move := range game.Moves(*chip, ship) {
		if game.Desk[move.To].Type == ARROW {
			continue
		}
		state := Reduce(game, Action{Type: MOVE, To: move.To, Level: move.Level, ChipIDs: []uint{chip.ID}})
		assert.NotEqual(t, state, game)
		assert.NotNil(t, state)
		cell := state.Chips[chip.ID].Cell
		assert.Equal(t, cell, move.To)
		assert.True(t, state.Desk[cell].Opened)
	}
	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerMoveShip(t *testing.T) {
	game := NewGame()
	dump, err := json.Marshal(game)
	utils.PErr(err)
	stype := SHIP
	ship := game.FilterChips(ChipFilter{Type: &stype, Owner: &game.Player})[0]

	chips := game.FilterChips(ChipFilter{Owner: &game.Player})
	assert.Len(t, chips, 4)
	chipIDs := make([]uint, len(chips))
	for i, chip := range chips {
		chipIDs[i] = chip.ID
	}
	for _, move := range game.Moves(*ship, ship) {
		state := Reduce(game, Action{Type: MOVE, To: move.To, Level: move.Level, ChipIDs: chipIDs})
		assert.NotEqual(t, state, game)
		for _, id := range chipIDs {
			cell := state.Chips[id].Cell
			to := game.Desk[move.To]
			onearrowCase := to.Type == ARROW && to.Level == 1
			assert.True(t, cell == move.To || onearrowCase)
		}
	}
	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerAutoMove1Arrow(t *testing.T) {
	game := NewGame()
	first := true
	for i, cell := range game.Desk {
		if cell.Type == ARROW && cell.Level == 1 && first {
			first = false
			continue
		}
		if cell.Type == ARROW || cell.Type == STAR || cell.Type == HEDGEHOG || cell.Type == VORTEX {
			game.Desk[i].Type = EMPTY
		}
	}
	dump, err := json.Marshal(game)
	utils.PErr(err)

	atype := ARROW
	level := uint(1)
	arrow := game.FilterCells(CellFilter{Type: &atype, Level: &level})[0]
	state := Reduce(game, Action{Type: MOVE, To: arrow.ID, ChipIDs: []uint{game.Chips[1].ID}})

	chip := state.Chips[1]
	cell := arrow
	for id := cell.Arrow().MustMoveTo(); ; id = cell.Arrow().MustMoveTo() {
		cell = &game.Desk[id]
		if cell.Type != ARROW || cell.Level != 1 {
			break
		}
	}
	assert.Equal(t, chip.Cell, cell.ID)

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerMarsh(t *testing.T) {
	game := NewGame()
	for i, cell := range game.Desk {
		if cell.Type == ARROW || cell.Type == STAR || cell.Type == HEDGEHOG || cell.Type == VORTEX {
			game.Desk[i].Type = EMPTY
		}
	}
	dump, err := json.Marshal(game)
	utils.PErr(err)

	for _, marsh := range game.CellsOfType(MARSH) {
		level := marsh.Level
		chipIDs := []uint{1}
		states := []*Game{Reduce(game, Action{Type: MOVE, To: marsh.ID, ChipIDs: chipIDs})}
		for i := uint(0); i < level; i++ {
			state := states[i]
			state.Player = 0
			moves := state.Moves(state.Chips[1], &state.Chips[0])
			if i+1 < level {
				assert.Len(t, moves, 1)
			} else {
				assert.NotEqual(t, len(moves), 1)
			}
			move := moves[0]
			states = append(states, Reduce(state, Action{Type: MOVE, To: move.To, Level: move.Level, ChipIDs: chipIDs}))
			chip := states[i+1].Chips[1]
			if i+1 < level {
				assert.Equal(t, chip.Cell, marsh.ID)
			} else {
				assert.NotEqual(t, chip.Cell, marsh.ID)
			}
		}
	}

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerVertex(t *testing.T) {
	game := NewGame()
	// делаем поле из воды, водоворотов и пустышек
	for i, cell := range game.Desk {
		if cell.Type != WATER && cell.Type != VORTEX {
			game.Desk[i].Type = EMPTY
		}
	}
	dump, err := json.Marshal(game)
	utils.PErr(err)

	count := 0
	for _, vortex := range game.CellsOfType(VORTEX) {
		// ищем соседние клетки-пустышки
		dirs := utils.UintFilterBy([]uint{1, 12, 13, 14}, true, func(i uint) bool {
			t1 := game.Desk[vortex.ID+i].Type
			t2 := game.Desk[vortex.ID-i].Type
			//spew.Dump(game.Desk[vortex.ID+i], game.Desk[vortex.ID-i])
			return t1 != WATER && t1 != VORTEX && t2 != WATER && t2 != VORTEX
		})
		if len(dirs) == 0 {
			continue
		}
		dir := *dirs[0]
		stateBefore := Reduce(game, Action{Type: MOVE, To: vortex.ID - dir, ChipIDs: []uint{1}})
		//moves := stateBefore.Moves(stateBefore.Chips[1], &stateBefore.Chips[0])
		//println("\nstart move to vertex")
		state := Reduce(stateBefore, Action{Type: MOVE, To: vortex.ID, ChipIDs: []uint{1}})
		//println("end move through vertex\n")
		cell := state.Chips[1].Cell
		expected := vortex.ID + dir
		if !assert.Equal(t, cell, expected) {
			//spew.Dump(game)
		}
		count += 1
	}
	assert.NotEqual(t, count, 0)

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerOpenCell(t *testing.T) {
	game := NewGame()
	dump, err := json.Marshal(game)
	utils.PErr(err)

	empty := game.CellsOfType(EMPTY)[0]
	state := Reduce(game, Action{Type: MOVE, To: empty.ID, ChipIDs: []uint{1}})
	assert.True(t, state.Desk[empty.ID].Opened)

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerKnockOut(t *testing.T) {
	game := NewGame()
	dump, err := json.Marshal(game)
	utils.PErr(err)

	empty := game.CellsOfType(EMPTY)[0]
	action := func(id uint) Action { return Action{Type: MOVE, To: empty.ID, ChipIDs: []uint{id}} }
	stateBefore := Reduce(game, action(1))
	state := Reduce(stateBefore, action(5))
	cell := state.Chips[1].Cell
	home := state.Chips[0].Cell
	enemy := state.Chips[5].Cell
	assert.Equal(t, cell, home)
	assert.Equal(t, enemy, empty.ID)

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerKnockOutCoin(t *testing.T) {
	game := NewGame()
	dump, err := json.Marshal(game)
	utils.PErr(err)

	treasure := game.CellsOfType(TREASURE)[0]
	state1 := Reduce(game, Action{Type: MOVE, ChipIDs: []uint{1}, To: treasure.ID})
	state2 := Reduce(state1, Action{Type: MOVE, ChipIDs: []uint{5}, To: treasure.ID})
	ctype := COIN_CHIP
	coins := state2.FilterChips(ChipFilter{Type: &ctype, Cell: &treasure.ID})
	assert.Len(t, coins, int(treasure.Level))

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerAssignOwnerToDonkeyAndFriday(t *testing.T) {
	game := NewGame()
	dump, err := json.Marshal(game)
	utils.PErr(err)

	donkey := game.CellsOfType(DONKEY)[0]
	state := Reduce(game, Action{Type: MOVE, To: donkey.ID, ChipIDs: []uint{1}})
	chip := state.ChipsOfType(DONKEY_CHIP)[0]
	if assert.NotNil(t, chip.Owner) {
		assert.Equal(t, *chip.Owner, game.Player)
	}

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerKnockOutDonkeyToDonkeyCell(t *testing.T) {
	game := NewGame()
	dump, err := json.Marshal(game)
	utils.PErr(err)

	donkeyCell := game.CellsOfType(DONKEY)[0]
	state1 := Reduce(game, Action{Type: MOVE, To: donkeyCell.ID, ChipIDs: []uint{1}})
	state1.Player = 0
	empty := game.CellsOfType(EMPTY)[0]
	donkey := state1.ChipsOfType(DONKEY_CHIP)[0]
	state2 := Reduce(state1, Action{Type: MOVE, To: empty.ID, ChipIDs: []uint{donkey.ID}})
	state := Reduce(state2, Action{Type: MOVE, To: empty.ID, ChipIDs: []uint{6}})
	cell := state.Chips[donkey.ID].Cell
	home := donkeyCell.ID
	assert.Equal(t, cell, home)

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerShouldBlock(t *testing.T) {
	game := NewGame()
	dump, err := json.Marshal(game)
	utils.PErr(err)

	btype := BLOCK
	two := uint(2)
	block := game.FilterCells(CellFilter{Type: &btype, Level: &two})[0]
	assert.Equal(t, block.Level, uint(2))
	mv := func(state *Game, chipId uint, toP *uint) (ret *Game) {
		to := block.ID
		if toP != nil {
			to = *toP
		}
		ret = Reduce(state, Action{Type: MOVE, To: to, ChipIDs: []uint{chipId}})
		ret.Player = 0
		return
	}
	ms := func(s *Game, id uint) []Move {
		return s.Moves(
			s.Chips[id],
			&s.Chips[0],
		)
	}
	zero := uint(0)
	s1 := mv(game, 1, nil)                                       // фишки один сцапана осьминогом
	assert.True(t, s1.Desk[block.ID].Active)                     // осьминог активировал свои щупальцы
	ms1 := ms(s1, 1)                                             // считаем доступные ходы
	assert.Len(t, ms1, 0)                                        // доступные ходы не обнаружены
	s2 := mv(s1, 2, nil)                                         // фишка 2 встала на осьминога
	assert.False(t, s2.Desk[block.ID].Active)                    // осьминог испугался и деактивировался
	ms2 := ms(s2, 2)                                             // считаем доступные ходы второй фишки
	ms21 := ms(s2, 1)                                            // и первой фишки
	assert.NotEmpty(t, ms2)                                      // ходы есть
	assert.NotEmpty(t, ms21)                                     // ходы есть
	s3 := mv(s2, 1, &zero)                                       // первая фишка уходит
	assert.False(t, s3.Desk[block.ID].Active)                    // осьминог всё ещё боится
	s4 := mv(s3, 2, &zero)                                       // второй уходит
	assert.Empty(t, s4.FilterChips(ChipFilter{Cell: &block.ID})) // никого на осьминоге не осталось
	s5 := mv(s4, 1, nil)                                         // первый возвращается на пустого осьминога
	s6 := mv(s3, 1, nil)                                         // первый вернулся к ещё не ушеднему второму
	assert.True(t, s5.Desk[block.ID].Active)                     // осьминог перестал бояться
	assert.False(t, s6.Desk[block.ID].Active)                    // осьминог всё ещё боится

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerShouldSinkOnShark(t *testing.T) {
	game := NewGame()
	dump, err := json.Marshal(game)
	utils.PErr(err)

	shark := game.CellsOfType(SHARK)[0]
	state := Reduce(game, Action{Type: MOVE, ChipIDs: []uint{1}, To: shark.ID})
	assert.Equal(t, state.Chips[1].Cell, shark.ID)
	assert.True(t, state.Chips[1].Sinked)

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerShouldSinkOnNonSafeCells(t *testing.T) {
	game := NewGame()
	dump, err := json.Marshal(game)
	utils.PErr(err)

	treasure := game.CellsOfType(TREASURE)[0]
	empty := game.CellsOfType(EMPTY)[0]
	water := game.Desk[0]
	state := Reduce(game, Action{Type: MOVE, To: treasure.ID, ChipIDs: []uint{1}})
	state.Player = 0
	ctype := COIN_CHIP
	coin := state.FilterChips(ChipFilter{Type: &ctype, Cell: &treasure.ID})[0]
	stateSinked := Reduce(state, Action{Type: MOVE, To: water.ID, ChipIDs: []uint{1, coin.ID}})
	stateAlive := Reduce(state, Action{Type: MOVE, To: empty.ID, ChipIDs: []uint{1, coin.ID}})
	assert.True(t, stateSinked.Chips[coin.ID].Sinked)
	assert.False(t, stateAlive.Chips[coin.ID].Sinked)

	stateShip := Reduce(state, Action{Type: MOVE, To: game.Chips[0].Cell, ChipIDs: []uint{1, coin.ID}})
	assert.False(t, stateShip.Chips[coin.ID].Sinked) // не должна тонуть на корабле
	assert.Equal(t, stateShip.Chips[coin.ID].Cell, game.Chips[0].Cell)
	assert.Equal(t, stateShip.Chips[1].Cell, game.Chips[0].Cell)

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerSelectOnShip(t *testing.T) {
	game := NewGame()
	dump, err := json.Marshal(game)
	utils.PErr(err)

	state := Reduce(game, Action{Type: SELECT, ID: uint(1)})
	assert.True(t, state.Chips[1].Selected)
	state2 := Reduce(state, Action{Type: SELECT, ID: uint(2)})
	assert.True(t, state2.Chips[2].Selected)
	assert.False(t, state2.Chips[1].Selected)
	state3 := Reduce(state2, Action{Type: SELECT, ID: uint(0)})
	assert.True(t, state3.Chips[0].Selected)
	assert.True(t, state3.Chips[1].Selected)
	assert.True(t, state3.Chips[2].Selected)
	assert.True(t, state3.Chips[3].Selected)
	state4 := Reduce(state3, Action{Type: SELECT, ID: uint(0)})
	assert.False(t, state4.Chips[0].Selected)
	assert.False(t, state4.Chips[1].Selected)
	assert.False(t, state4.Chips[2].Selected)
	assert.False(t, state4.Chips[3].Selected)
	state5 := Reduce(state3, Action{Type: SELECT, ID: uint(1)})
	assert.False(t, state5.Chips[0].Selected)
	assert.True(t, state5.Chips[1].Selected)
	assert.False(t, state5.Chips[2].Selected)
	assert.False(t, state5.Chips[3].Selected)

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}

func TestReducerSelectWithCoins(t *testing.T) {
	game := NewGame()
	dump, err := json.Marshal(game)
	utils.PErr(err)

	ttype := TREASURE
	ctype := COIN_CHIP
	five := uint(5)
	treasure := game.FilterCells(CellFilter{Type: &ttype, Level: &five})[0]
	coin := game.FilterChips(ChipFilter{Type: &ctype, Cell: &treasure.ID})[0]
	state1 := Reduce(game, Action{Type: MOVE, To: treasure.ID, ChipIDs: []uint{1}})
	state1.Player = 0
	state2 := Reduce(state1, Action{Type: SELECT, ID: uint(1)})
	assert.True(t, state2.Chips[1].Selected)
	state3 := Reduce(state2, Action{Type: SELECT, ID: coin.ID})
	assert.True(t, state3.Chips[1].Selected)
	assert.True(t, state3.Chips[coin.ID].Selected)
	state4 := Reduce(state3, Action{Type: SELECT, ID: 2})
	assert.False(t, state4.Chips[1].Selected)
	assert.True(t, state4.Chips[2].Selected)
	assert.False(t, state4.Chips[coin.ID].Selected)

	newDump, err := json.Marshal(game)
	utils.PErr(err)
	assert.True(t, len(newDump) > 0)
	assert.Equal(t, string(newDump), string(dump))
}
