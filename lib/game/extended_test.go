package game

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExtendedNoPanic(t *testing.T) {
	assert.NotPanics(t, func() {
		NewGame().Extend()
	})
}

func TestExtendedDonkeyMoves(t *testing.T) {
	game := NewGame()
	donkeyCell := game.CellsOfType(DONKEY)[0]
	state1 := Reduce(game, Action{Type: MOVE, To: donkeyCell.ID, ChipIDs: []uint{1}})
	state1.Player = 0
	empty := game.CellsOfType(EMPTY)[0]
	donkey := state1.ChipsOfType(DONKEY_CHIP)[0]
	state2 := Reduce(state1, Action{Type: MOVE, To: empty.ID, ChipIDs: []uint{donkey.ID}})
	extended := state2.Extend()
	if assert.Equal(t, len(extended.Desk[empty.ID].Chips), 1) {
		assert.Equal(t, extended.Desk[empty.ID].Chips[0].ID, donkey.ID)
	}

}
