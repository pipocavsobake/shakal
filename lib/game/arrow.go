package game

import "fmt"

type Arrow struct {
	ID, Level, Direction uint
	Neighbors            []uint
}

func NewArrow(id, level, direction uint) (arrow *Arrow) {
	if level != 1 && level != 2 && level != 4 && level != 8 {
		panic(fmt.Sprintf("bad level %v", level))
	}
	if direction > 8/level {
		panic(fmt.Sprintf("bad direction %v for level %v", direction, level))
	}
	arrow = &Arrow{ID: id, Level: level, Direction: direction}
	arrow.Neighbors = make([]uint, 0)
	for _, delta := range []int{-13, -12, 1, 14, 13, 12, -1, -14} {
		neighbor := delta + int(id)
		if neighbor < 0 {
			panic(fmt.Sprintf("neighbor (%v) < 0", neighbor))
		}
		arrow.Neighbors = append(arrow.Neighbors, uint(neighbor))
	}
	return
}

func (arrow *Arrow) CanMoveTo() (cells []uint) {
	cells = make([]uint, 0)
	for i, neighbor := range arrow.Neighbors {
		delta := i - int(arrow.Direction)
		if delta < 0 {
			delta *= -1
		}
		if uint(delta)%(8/arrow.Level) == 0 {
			cells = append(cells, neighbor)
		}
	}
	return
}

func (arrow *Arrow) MustMoveTo() (cell uint) {
	if arrow.Level != 1 {
		panic("arrow.Level != 1")
	}
	return arrow.CanMoveTo()[0]
}

func (cell *Cell) Arrow() *Arrow {
	if cell.Type != ARROW {
		panic("cell.Type != ARROW")
	}
	return NewArrow(cell.ID, cell.Level, cell.Direction)
}
