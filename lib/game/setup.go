package game

import (
	"math/rand"
	"time"
)

type CellType uint

const (
	MARSH CellType = iota + 1
	TREASURE
	ARROW
	STAR
	HEDGEHOG
	EMPTY
	HORSE
	SHARK
	DONKEY
	FRIDAY
	VORTEX
	BLOCK
	ISLAND
	WATER
)

type FieldSetup struct {
	Type       CellType
	Level      uint
	Count      uint
	Directions uint
	Active     bool
}

func init() {
	rand.Seed(time.Now().UnixNano())
}

func (field *FieldSetup) Direction() uint {
	if field.Directions == 0 {
		return 0
	}
	return uint(rand.Intn(int(field.Directions)))
}

func (field *FieldSetup) Generate() Cell {
	return Cell{
		Opened:       false,
		SafeForCoins: field.Type.SafeForCoins(),
		Direction:    field.Direction(),
		Type:         field.Type,
		Level:        field.Level,
		Active:       field.Active,
	}
}

func Fields() []FieldSetup {
	return []FieldSetup{
		{Type: MARSH, Level: 2, Count: 7},
		{Type: MARSH, Level: 3, Count: 6},
		{Type: MARSH, Level: 4, Count: 5},
		{Type: MARSH, Level: 5, Count: 4},
		{Type: MARSH, Level: 6, Count: 2},
		{Type: TREASURE, Level: 1, Count: 5},
		{Type: TREASURE, Level: 2, Count: 4},
		{Type: TREASURE, Level: 3, Count: 3},
		{Type: TREASURE, Level: 4, Count: 2},
		{Type: TREASURE, Level: 5, Count: 1},
		{Type: ARROW, Level: 1, Count: 8, Directions: 7},
		{Type: ARROW, Level: 2, Count: 8, Directions: 3},
		{Type: ARROW, Level: 4, Count: 8, Directions: 1},
		{Type: ARROW, Level: 8, Count: 6},
		{Type: STAR, Count: 3, Directions: 3},
		{Type: HEDGEHOG, Count: 1}, // ёж
		{Type: EMPTY, Count: 18},
		{Type: HORSE, Count: 5},
		{Type: SHARK, Count: 1},
		{Type: DONKEY, Count: 1},
		{Type: FRIDAY, Count: 1},
		{Type: VORTEX, Count: 5},
		{Type: BLOCK, Level: 2, Count: 3, Active: true},
		{Type: BLOCK, Level: 3, Count: 1, Active: true},
		{Type: ISLAND, Count: 13},
	}
}

func (cellType CellType) SafeForCoins() bool {
	if VORTEX == cellType || ISLAND == cellType || WATER == cellType {
		return false
	}
	return true
}

type Cell struct {
	Type         CellType `json:"type"`
	Level        uint     `json:"level"`
	Active       bool     `json:"active"`
	Opened       bool     `json:"opened"`
	SafeForCoins bool     `json:"safeForCoins"`
	Direction    uint     `json:"direction"`
	ID           uint     `json:"id"`
	Owner        *uint    `json:"owner"`
	Chips        []Chip   `json:"objects"`
	Achievable   bool     `json:"achievable"`
}

func Internal() (cells []Cell) {
	cells = make([]Cell, 0)
	fields := Fields()
	for _, field := range fields {
		for i := uint(0); i < field.Count; i++ {
			cells = append(cells, field.Generate())
		}
	}
	return
}

type ChipType uint

const (
	SHIP ChipType = iota + 1
	CHIP
	COIN_CHIP
	FRIDAY_CHIP
	DONKEY_CHIP
)

type Chip struct {
	Type       ChipType `json:"chip_type"`
	Level      uint     `json:"level"`
	Opened     bool     `json:"opened"`
	ID         uint     `json:"id"`
	Owner      *uint    `json:"owner"`
	Cell       uint     `json:"cell"`
	Sinked     bool     `json:"sinked"`
	Selected   bool     `json:"selected"`
	Selectable bool     `json:"selectable"`
}

// put owner's chips on ship, ship on cell
func ShipsWithChips(owner, cell uint) (chips []Chip) {
	chips = make([]Chip, 4)
	chips[0] = Chip{Type: SHIP, Owner: &owner, ID: owner * 4, Cell: cell}
	for i := 1; i < 4; i++ {
		chips[i] = Chip{Type: CHIP, Owner: &owner, ID: owner*4 + uint(i), Cell: cell}
	}
	return
}

func Coins(cell Cell, count uint) (chips []Chip) {
	chips = make([]Chip, 0)
	for id := count; id < count+cell.Level; id++ {
		chips = append(chips, Chip{Type: COIN_CHIP, ID: id, Opened: false, Cell: cell.ID})
	}
	return
}

func GenWater() Cell {
	return Cell{Type: WATER, SafeForCoins: false, Opened: true}
}

func Desk() (cells []Cell) {
	cells = make([]Cell, 0)
	inter := Internal()
	rand.Shuffle(len(inter), func(i, j int) {
		tmp := inter[i]
		inter[i] = inter[j]
		inter[j] = tmp
	})
	for i := 0; i < 13; i++ {
		cells = append(cells, GenWater())
	}
	for i := 0; i < 11; i++ {
		cells = append(cells, GenWater())
		cells = append(cells, inter[11*i:11*(i+1)]...)
		cells = append(cells, GenWater())
	}
	for i := 0; i < 13; i++ {
		cells = append(cells, GenWater())
	}
	for i, _ := range cells {
		cells[i].ID = uint(i)
	}
	return
}

func Chips(dsk []Cell) (chips []Chip) {
	chips = make([]Chip, 0)
	treasures := make([]Cell, 0)
	var fridayCell uint
	var donkeyCell uint
	shipsChips := make([]Chip, 0)
	for _, cell := range dsk {
		switch cell.Type {
		case TREASURE:
			treasures = append(treasures, cell)
			break
		case FRIDAY:
			fridayCell = cell.ID
			break
		case DONKEY:
			donkeyCell = cell.ID
			break
		default:
			continue
		}
	}
	for owner, cell := range []uint{5, 5 * 13, 163, 6*13 - 1} {
		shipsChips = append(shipsChips, ShipsWithChips(uint(owner), cell)...)
	}
	count := uint(len(shipsChips))
	chips = append(chips, shipsChips...)
	chips = append(chips, Chip{Type: FRIDAY_CHIP, Cell: fridayCell, ID: count, Opened: false})
	count += 1
	chips = append(chips, Chip{Type: DONKEY_CHIP, Cell: donkeyCell, ID: count, Opened: false})
	count += 1
	for _, cell := range treasures {
		localCoins := Coins(cell, count)
		chips = append(chips, localCoins...)
		count += uint(len(localCoins))
	}
	return
}
