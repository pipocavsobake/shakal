package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUinFilterBy(t *testing.T) {
	a := []uint{1, 2, 3, 4, 5, 6, 7}
	b := UintFilterBy(a, true, func(e uint) bool {
		return e == 2 || e == 6
	})
	if assert.Len(t, b, 2) {
		assert.Equal(t, *b[0], uint(2))
		assert.Equal(t, *b[1], uint(6))
	}
}
