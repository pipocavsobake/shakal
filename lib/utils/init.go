package utils

import (
	"fmt"
	"os"
)

func PErr(err error) {
	if err != nil {
		panic(err)
	}
}

func TestOnly() {
	if env := os.Getenv("RAILS_ENV"); env != "test" {
		panic(fmt.Sprintf("You cannot run this at RAILS_ENV=%v; please, provide RAILS_ENV=test", env))
	}

}
func UintIncludes(a []uint, e uint) bool {
	for _, v := range a {
		if v == e {
			return true
		}
	}
	return false
}

func UintFilterBy(a []uint, Copy bool, filter func(uint) bool) []*uint {
	ret := []*uint{}
	for i, e := range a {
		if filter(e) {
			if Copy {
				re := e
				ret = append(ret, &re)
			} else {
				ret = append(ret, &a[i])
			}
		}
	}
	return ret
}
